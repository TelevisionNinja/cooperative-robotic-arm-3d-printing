import sys
import rospy
import moveit_commander
import moveit_msgs.msg


class MoveItTrajectories(object):
  def __init__(self):

    # Initialize the node
    super(MoveItTrajectories, self).__init__()
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("example_move_it_trajectories")

    try:        
      self.gripper_joint_name = ""
      self.degrees_of_freedom = rospy.get_param(rospy.get_namespace() + "degrees_of_freedom", 7)

      # Create the MoveItInterface necessary objects
      arm_group_name = "arm"
      self.robot = moveit_commander.RobotCommander("robot_description")
      self.scene = moveit_commander.PlanningSceneInterface(ns=rospy.get_namespace())
      self.arm_group = moveit_commander.MoveGroupCommander(arm_group_name, ns=rospy.get_namespace())
      self.display_trajectory_publisher = rospy.Publisher(
        rospy.get_namespace() + 'move_group/display_planned_path',
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20
      )

      rospy.loginfo("Initializing node in namespace " + rospy.get_namespace())
    except Exception as e:
      print (e)
      self.is_init_success = False
    else:
      self.is_init_success = True


  def reach_named_position(self, target):
    arm_group = self.arm_group
    
    # Going to one of those targets
    rospy.loginfo("Going to named target " + target)
    # Set the target
    arm_group.set_named_target(target)
    # Plan the trajectory
    (success_flag, trajectory_message, planning_time, error_code) = arm_group.plan()
    # Execute the trajectory and block while it's not finished
    return arm_group.execute(trajectory_message, wait=True)


  def get_cartesian_pose(self):
    arm_group = self.arm_group

    # Get the current pose and display it
    pose = arm_group.get_current_pose()
    rospy.loginfo("Actual cartesian pose is : ")
    rospy.loginfo(pose.pose)

    return pose.pose


  def reach_cartesian_pose(self, pose, tolerance, constraints):
    arm_group = self.arm_group
    
    # Set the tolerance
    arm_group.set_goal_position_tolerance(tolerance)

    # Set the trajectory constraint if one is specified
    if constraints is not None:
      arm_group.set_path_constraints(constraints)

    # Get the current Cartesian Position
    arm_group.set_pose_target(pose)

    # Plan and execute
    rospy.loginfo("Planning and going to the Cartesian Pose")
    return arm_group.go(wait=True)


def goToCoordinate(trajectories, homepose, abosolutePosition =False, x=0, y=0, z=0):
  actual_pose = trajectories.get_cartesian_pose()

  if (x is None):
    x=0
  if (y is None):
    y=0
  if (z is None):
    z=0

  if abosolutePosition:
    actual_pose.position.x = homepose.position.x + x * (1 - homepose.position.x) * 0.5
    actual_pose.position.y = homepose.position.y + y * (1 - homepose.position.y) * 0.5
    actual_pose.position.z = homepose.position.z + z * (1 - homepose.position.z) * 0.5
  else:
    actual_pose.position.x += x
    actual_pose.position.y += y
    actual_pose.position.z += z

  trajectories.reach_cartesian_pose(pose=actual_pose, tolerance=0.001, constraints=None)


# for extracting gcode
import re

# for opening files
import sys
import os


extractGCodeRegex = re.compile(r"(?:(G\d+( [a-w][0-9]+)*))((?: )(x)(-?\d+(\.\d+)?))+((?: )(y)(-?\d+(\.\d+)?))((?: )(z)(-?\d+(\.\d+)?))?", re.IGNORECASE)
parameters = {'x', 'y', 'z'}


def extract(gCode):
    """
    example of a return value:

    [
        [x, 0],
        [y, 0],
        [z, 0],
        [x, 0],
        [y, 0],
        [z, 0],
        ...
    ]
    """

    coordinateMatches = extractGCodeRegex.findall(gCode.lower())
    coordinates = []

    for match in coordinateMatches:
        isCoordinate = False
        coordinate = []

        for group in match:
            if group in parameters:
                coordinate.append(group)
                isCoordinate = True
            elif isCoordinate:
                isCoordinate = False
                coordinate.append(group)
                coordinates.append(coordinate)
                coordinate = []

    return coordinates


def extractFile(fileName):
    coordinates = []

    if os.path.isfile(fileName):
        with open(fileName) as file:
            coordinates = extract(file.read())

    return coordinates


def convertPositionToDistance(arr):
    """
    this function loops through the array of letter (x,y,z) and number pairs and 
    for each pair and checks what the letter is in the first position, and if it is
    a 'x', then it puts that pair in the first, (x, , ), location in the position.
    for 'y', it adds it to the second, ( ,y, ), location in the position.
    it loops adding x, y, and z values to the postion until it finds the 'x' value for
    the next coordinate. When that happens, it appends the positionArray with the
    current position.

    With that array, it will then take every position and divide it by the max value(1000 is hard-coded in right now)
    to get a value between 0 and 1, which is what is needed by the kinova arm.

    using the positionArry shown above it looks like this:
    [
        [
            (123 / 1000),
            (456 / 1000),
            (789 / 1000)
        ],
        ......
    ]
    """

    position = [0, 0, 0]
    positionArray = []

    for curr in arr:
        if (curr[0] == 'x'):
            position[0] = float(curr[1]) / 190
        if (curr[0] == 'y'):
            position[1] = float(curr[1]) / 190
        if (curr[0] == 'z'):
            position[2] = float(curr[1]) / 190
        if (curr[0] == 'x'):
            if(position[2] == []):
                position[2] = '0'
            positionArray.append(position)
            position = [0, 0, 0]

    return positionArray


def main():
    example = MoveItTrajectories()

    # For testing purposes
    success = example.is_init_success
    try:
        rospy.delete_param("/kortex_examples_test_results/moveit_general_python")
    except:
        pass

    homepose = None

    if success:
        rospy.loginfo("Reaching Named Target Home...")
        success &= example.reach_named_position("home")
        homepose = example.get_cartesian_pose()
        print (success)
    
    directory = os.path.realpath(__file__)
    directory = os.path.dirname(directory)

    coordinates = []

    with open(directory + "/cube.gcode") as file:
        coordinates = extract(file.read())
        coordinates = convertPositionToDistance(coordinates)

    for coordinate in coordinates:
        goToCoordinate(example, homepose, True, coordinate[0], coordinate[1], coordinate[2])

    if not success:
        rospy.logerr("The example encountered an error.")

if __name__ == '__main__':
  main()
