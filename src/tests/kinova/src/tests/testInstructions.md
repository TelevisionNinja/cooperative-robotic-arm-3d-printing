Replace the contents of the
```
catkin_workspace/src/ros_kortex/kortex_examples/src/move_it/example_move_it_trajectories.py
```
file with the contents of the simplePathTest.py file.

## Launch ROS 1 Gazebo with the Kinova Arm:
```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch kortex_gazebo spawn_kortex_robot.launch
```

## Launch the ROS 1 Kinova Arm MoveIt Example:
```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch kortex_examples moveit_example.launch
```
