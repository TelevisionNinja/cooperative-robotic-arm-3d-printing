import sys
import time
import rospy
import moveit_commander
import moveit_msgs.msg


class MoveItTrajectories(object):
  def __init__(self):

    # Initialize the node
    super(MoveItTrajectories, self).__init__()
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node("example_move_it_trajectories")

    try:        
      self.gripper_joint_name = ""
      self.degrees_of_freedom = rospy.get_param(rospy.get_namespace() + "degrees_of_freedom", 7)

      # Create the MoveItInterface necessary objects
      arm_group_name = "arm"
      self.robot = moveit_commander.RobotCommander("robot_description")
      self.scene = moveit_commander.PlanningSceneInterface(ns=rospy.get_namespace())
      self.arm_group = moveit_commander.MoveGroupCommander(arm_group_name, ns=rospy.get_namespace())
      self.display_trajectory_publisher = rospy.Publisher(
        rospy.get_namespace() + 'move_group/display_planned_path',
        moveit_msgs.msg.DisplayTrajectory,
        queue_size=20
      )

      rospy.loginfo("Initializing node in namespace " + rospy.get_namespace())
    except Exception as e:
      print (e)
      self.is_init_success = False
    else:
      self.is_init_success = True


  def reach_named_position(self, target):
    arm_group = self.arm_group
    
    # Going to one of those targets
    rospy.loginfo("Going to named target " + target)
    # Set the target
    arm_group.set_named_target(target)
    # Plan the trajectory
    (success_flag, trajectory_message, planning_time, error_code) = arm_group.plan()
    # Execute the trajectory and block while it's not finished
    return arm_group.execute(trajectory_message, wait=True)


  def get_cartesian_pose(self):
    arm_group = self.arm_group

    # Get the current pose and display it
    pose = arm_group.get_current_pose()
    rospy.loginfo("Actual cartesian pose is : ")
    rospy.loginfo(pose.pose)

    return pose.pose


  def reach_cartesian_pose(self, pose, tolerance, constraints):
    arm_group = self.arm_group
    
    # Set the tolerance
    arm_group.set_goal_position_tolerance(tolerance)

    # Set the trajectory constraint if one is specified
    if constraints is not None:
      arm_group.set_path_constraints(constraints)

    # Get the current Cartesian Position
    arm_group.set_pose_target(pose)

    # Plan and execute
    rospy.loginfo("Planning and going to the Cartesian Pose")
    return arm_group.go(wait=True)


def goToCoordinate(trajectories, homepose, abosolutePosition =False, x=0, y=0, z=0):
  actual_pose = trajectories.get_cartesian_pose()

  if (x is None):
    x=0
  if (y is None):
    y=0
  if (z is None):
    z=0

  if abosolutePosition:
    actual_pose.position.x = homepose.position.x + x * (1 - homepose.position.x) * 0.5
    actual_pose.position.y = homepose.position.y + y * (1 - homepose.position.y) * 0.5
    actual_pose.position.z = homepose.position.z + z * (1 - homepose.position.z) * 0.5
  else:
    actual_pose.position.x += x
    actual_pose.position.y += y
    actual_pose.position.z += z

  trajectories.reach_cartesian_pose(pose=actual_pose, tolerance=0.001, constraints=None)


def main():
    example = MoveItTrajectories()

    # For testing purposes
    success = example.is_init_success
    try:
        rospy.delete_param("/kortex_examples_test_results/moveit_general_python")
    except:
        pass

    homepose = None
  
    # move in a line

    if success:
        rospy.loginfo("Reaching Named Target Home...")
        success &= example.reach_named_position("home")
        homepose = example.get_cartesian_pose()
        print (success)
    
    goToCoordinate(example, homepose, False, 0, 0.45, 0)

    if success:
        rospy.loginfo("Reaching Named Target Home...")
        success &= example.reach_named_position("home")
        
        print (success)

    # time.sleep(5)

    #===================================================================
    # move in a square

    goToCoordinate(example, homepose, False, 0, 0.25, 0)

    goToCoordinate(example, homepose, False, 0.25, 0, 0)

    goToCoordinate(example, homepose, False, 0, -0.25, 0)
    
    goToCoordinate(example, homepose, False, -0.25, 0, 0)

    rospy.loginfo("Reaching Named Target Home...")
    example.reach_named_position("home")

    goToCoordinate(example, homepose, True, 0, 0.25, 0)

    goToCoordinate(example, homepose, True, 0.25, 0.25, 0)

    goToCoordinate(example, homepose, True, 0.25, 0, 0)
    
    goToCoordinate(example, homepose, True, 0, 0, 0)


    # For testing purposes
    # rospy.set_param("/kortex_examples_test_results/moveit_general_python", success)

    if not success:
        rospy.logerr("The example encountered an error.")

if __name__ == '__main__':
  main()
