import pymesh
import subprocess

def cuttingMesh():

    userFilename = input("What file would you like to print?")

    #load initial stl files (user uploaded) and plane used for cutting
    halfMesh = pymesh.load_mesh("half.ply")
    userMesh = pymesh.load_mesh(userFilename)   

    #Making sure the Usermesh is centered (0,y,0) and that it is all on sitting on the positive side of the y axis
    num = 0
    #find minimum and maximum X vertex value, minimum Y vertex value, and minimum and maximum z vertex value
    for i in userMesh.vertices:
        count = 0
        for j in i:
            count = count + 1
            if count == 1:
                if num == 0:
                    minX = j
                    maxX = j
                else:
                    if j < minX:
                        minX = j
                    if j > maxX:
                        maxX = j
            elif count == 2:
                if num == 0:
                    minY = j
                else:
                    if j < minY:
                        minY = j
            else:
                if num == 0:
                    minZ = j
                    maxZ = j
                else:
                    if j <  minZ:
                        minZ = j
                    if j > maxZ:
                        maxZ = j
        num = num + 1

    #determining how y should be translated
    if minY > 0:
        yTranslation = minY
    elif minY < 0:
        yTranslation = -minY
    else:
        yTranslation = 0

    #determininy how x should be translated
    xAverage = (maxX + minX)/2.0
    if xAverage < 0:
        xTranslation = xAverage
    elif xAverage > 0:
        xTranslation = -xAverage
    else:
        xTranslation = 0

    #determining how z shoule be translated
    zAverage = (maxZ + minZ)/2.0
    if zAverage < 0:
        zTranslation = zAverage
    elif xAverage > 0:
        zTranslation = -zAverage
    else:
        zTranslation = 0

    #creating new mesh with translated vertices
    userMesh = pymesh.form_mesh(userMesh.vertices + [[xTranslation,yTranslation,zTranslation]], userMesh.faces)

    #boolean operations to cut mesh
    oneMesh = pymesh.boolean(halfMesh, userMesh, "intersection")
    twoMesh = pymesh.boolean(userMesh, halfMesh, operation="difference", engine="igl")

    #translate to correct coordinates
    oneMesh = pymesh.form_mesh(oneMesh.vertices + [[445.5, 445.5, 0]], oneMesh.faces)
    twoMesh = pymesh.form_mesh(twoMesh.vertices + [[445.4, 445.4, 0]], twoMesh.faces)

    #save stl files
    pymesh.save_mesh("oneSideOutput.stl", oneMesh, save_normal_vertex=False)
    pymesh.save_mesh("twoSideOutput.stl", twoMesh, save_normal_vertex=False)


def slicing():
    #runs the bash file that uses prusa slicer to slice into gcode
    subprocess.run(["./slicer.sh oneSideOutput.stl"], shell=True)
    subprocess.run(["./slicer.sh twoSideOutput.stl"], shell=True)


def main():
    cuttingMesh()
    slicing()

if __name__ == '__main__':
    main()

