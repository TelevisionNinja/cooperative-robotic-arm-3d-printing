from extractGCode import extract, convertPositions

gcode = """
G0 X-9.914 Y-9.843 z-9.843
G0 X-9.914 Y-9.843
G1 F100 X-9.914 Y-9.843 E3.36222
G0 F9000 X-9.843 Y-9.914 Z-9.914
G1 F1200 X9.914 Y9.843 Z9.843 E3.65844
G0 F9000 X9.914 Y9.702
G2
G1 a213123 e123213 y21398123
G1 a213123 a123213 a21398123

G1 1231231 e123213 y21398123
G1 a e123213 y21398123
"""

parameters = extract(gcode)

for parameter in parameters:
    print(parameter)

print()

converted = convertPositions(parameters)

for convertedCommand in converted:
    print(convertedCommand)
