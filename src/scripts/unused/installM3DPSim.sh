#! /bin/bash

# install gazebo dependencies
sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
wget https://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
sudo apt-get update
wget https://raw.githubusercontent.com/ignition-tooling/release-tools/master/jenkins-scripts/lib/dependencies_archive.sh -O /tmp/dependencies.sh
GAZEBO_MAJOR_VERSION=11 ROS_DISTRO=galactic . /tmp/dependencies.sh
echo $BASE_DEPENDENCIES $GAZEBO_BASE_DEPENDENCIES | tr -d '\\' | xargs sudo apt-get -y install

# install build essentials
sudo apt update
sudo apt install build-essentials 

# install M3DP-sim's provided gazebo
git clone https://bitbucket.org/hbpneurorobotics/gazebo.git
cd gazebo
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local ..
make -j8
make install

### Install Gazebo Fluid Simulation Plugin with SPlisHSPlasH
echo export LD_LIBRARY_PATH=/home/jason2/.local/lib:$LD_LIBRARY_PATH:/home/$1/splisplash/build/lib >> ~/.bashrc

source ~/.bashrc


git clone https://github.com/ai4ce/M3DP-Sim.git


git clone https://github.com/doyubkim/fluid-engine-dev
cd fluid-engine-dev/
mkdir build && cd build && cmake .. && make
