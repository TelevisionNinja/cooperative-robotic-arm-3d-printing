source /opt/ros/galactic/setup.bash
cd ~/workspace/ros_ws_galactic
source install/setup.bash
ros2 launch ur_simulation_gazebo ur_sim_moveit.launch.py ur_type:=ur10 launch_rviz:=false
