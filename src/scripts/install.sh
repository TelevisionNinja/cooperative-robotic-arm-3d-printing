#! /bin/bash

# Install ROS 1 Noetic:
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install ros-noetic-desktop-full
source /opt/ros/noetic/setup.bash
sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
sudo apt install python3-rosdep
sudo rosdep init
rosdep update

# Install MoveIt for ROS 1 Noetic:
sudo apt install ros-noetic-moveit

# Install Kinova Robotics for ROS 1 Noetic:
source /opt/ros/noetic/setup.bash
sudo apt install python3 python3-pip
sudo python3 -m pip install conan
conan config set general.revisions_enabled=1
conan profile new default --detect > /dev/null
conan profile update settings.compiler.libcxx=libstdc++11 default
mkdir -p catkin_workspace/src
cd catkin_workspace/src
git clone -b noetic-devel https://github.com/Kinovarobotics/ros_kortex.git

# Clone ambots repo:
git clone https://gitlab.com/capstone-team16/ambots.git

cd ../
rosdep install --from-paths src --ignore-src -y --rosdistro noetic
catkin_make

source devel/setup.bash

# Install Universal Robots for ROS 1 Noetic:
sudo apt-get install ros-noetic-universal-robots

# Install PyMesh
git clone https://github.com/PyMesh/PyMesh.git
cd PyMesh
git submodule update --init
sudo apt-get purge python
sudo apt-get install \
    libeigen3-dev \
    libgmp-dev \
	libgmpxx4ldbl \
	libmpfr-dev \
	libboost-dev \
	libboost-thread-dev \
	libtbb-dev \
	python3-dev \
	python3-setuptools \
	python3-numpy \
	python3-scipy \
	python3-nose \
	python3-pip \
	cmake

python3 ./setup.py build
python3 ./setup.py install --user
python3 -c "import pymesh; pymesh.test()"

# Install PrusaSlicer
sudo snap install prusa-slicer

# Make the slicing script executable
cd ../
cd ./src/ambots/src/catkinWorkspace/src/ambots/frontEnd/
sudo chmod +x slicer.sh

# Make the launch scripts executable
cd ../../../../
cd ./scripts/
sudo chmod +x launchCommunicationHub.sh
sudo chmod +x launchFrontEnd.sh
sudo chmod +x launchKinovaFirmware.sh
sudo chmod +x launchUR10Firmware.sh
sudo chmod +x launchRviz.sh
