#!/usr/bin/env python3

# for opening files
import sys
import os
fileDirectory = os.path.realpath(__file__)
fileDirectory = os.path.dirname(fileDirectory)
fileDirectory = os.path.dirname(fileDirectory) # parent directory
sys.path.insert(0, fileDirectory)

# gcode
import extractGCode

# ros
import rospy
import moveit_commander
import moveit_msgs.msg

# extrusion simulation
import visualization_msgs.msg
import geometry_msgs.msg
import math
import tf.transformations
import shape_msgs.msg

# communication
import std_msgs.msg

# pause command
import threading


class Firmware(object):
  def __init__(self,
      name = "",
      arm_group_name = "",
      tfPrefix = "",
      xGcodeMax = 0, # mm
      yGcodeMax = 0, # mm
      zGcodeMax = 0, # mm
      xGcodeMin = 0, # mm
      yGcodeMin = 0, # mm
      zGcodeMin = 0, # mm
      xArmMax = 1.0,
      xArmMin = -1.0,
      yArmMax = 1.0,
      yArmMin = -1.0,
      zArmMax = 1.0,
      zArmMin = -1.0,
      xOffset = 0.0,
      yOffset = 0.0,
      zOffset = 0.0,
      filamentDiameter = 0.0, # meters
      maxVelocity = 0, # mm / s
      xConversion= None,
      yConversion= None,
      zConversion= None,
      tolerance= 0.0005, # meters
      filamentColor= {
        "r":1.0,
        "g":0.0,
        "b":0.0,
        "a":1.0
      },
      debug=False,
      topic= "",
      communication_hub_topic = "/ambots/communication_hub_topic",
      robotID = 0,
      eef_step = 0.01, # meters
      jump_threshold = 0.0, # set to 0 to ignore the check for infeasible jumps in joint space
      filamentXOffset = 0.0, # meters
      filamentYOffset = 0.0, # meters
      filamentZOffset = -0.1, # meters
      attachCollisionObjects = True
    ):

    self.name = name
    self.arm_group_name = arm_group_name
    self.tfPrefix = tfPrefix
    self.xGcodeMax = xGcodeMax
    self.yGcodeMax = yGcodeMax
    self.zGcodeMax = zGcodeMax
    self.xGcodeMin = xGcodeMin
    self.yGcodeMin = yGcodeMin
    self.zGcodeMin = zGcodeMin
    self.xArmMax = xArmMax
    self.xArmMin = xArmMin
    self.yArmMax = yArmMax
    self.yArmMin = yArmMin
    self.zArmMax = zArmMax
    self.zArmMin = zArmMin
    self.xOffset = xOffset
    self.yOffset = yOffset
    self.zOffset = zOffset
    self.filamentDiameter = filamentDiameter
    self.maxVelocity = maxVelocity
    self.tolerance = tolerance
    self.filamentColor = filamentColor
    self.debug = debug
    self.topic_name = topic
    self.communication_hub_topic_name = communication_hub_topic
    self.robotID = robotID
    self.eef_step = eef_step
    self.jump_threshold = jump_threshold
    self.filamentXOffset = filamentXOffset
    self.filamentYOffset = filamentYOffset
    self.filamentZOffset = filamentZOffset

    self.msg_type = std_msgs.msg.String
    self.topic = rospy.Subscriber(self.topic_name, self.msg_type, self.handleMessage)
    self.communication_hub = rospy.Publisher(self.communication_hub_topic_name, self.msg_type, queue_size=10)

    if xConversion is not None:
      self.xConversion = xConversion
    if yConversion is not None:
      self.yConversion = yConversion
    if zConversion is not None:
      self.zConversion = zConversion

    # Initialize the node
    super(Firmware, self).__init__()
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node(name + "Firmware", anonymous=True)

    # Initialize markers to simulate printing
    self.marker_pub = rospy.Publisher(rospy.get_namespace() + "visualization_marker_array", visualization_msgs.msg.MarkerArray, queue_size=100)
    self.marker_array = visualization_msgs.msg.MarkerArray()
    self.markerCounter = 0
    self.isPrinting = False

    # Initialize other variables to simulate printing
    self.extrusionAmount = 0
    self.paused = False
    self.started = False
    self.gCodeChunk = ""
    self.extruder_name = "extruder"

    # save the last state to send to the other arm
    self.state = {}
    self.state["x"] = None
    self.state["y"] = None
    self.state["z"] = None

    # Create the MoveItInterface necessary objects
    self.robot = moveit_commander.RobotCommander(rospy.get_namespace() + "robot_description")
    self.scene = moveit_commander.PlanningSceneInterface(ns=rospy.get_namespace(), synchronous=True)
    self.arm_group = moveit_commander.MoveGroupCommander(self.arm_group_name, ns=rospy.get_namespace())

    self.arm_group.set_max_acceleration_scaling_factor(1)
    self.arm_group.set_max_velocity_scaling_factor(1)

    # move the arm to the home position
    self.reach_named_position("home")
    self.homePosition = self.arm_group.get_current_pose()

    # attach a visual representation of the extruder
    if attachCollisionObjects:
      self.attach_extruder()
      self.attach_ground()


  def reach_named_position(self, target):
    arm_group = self.arm_group
    
    # Going to one of those targets
    # Set the target
    arm_group.set_named_target(target)
    # Plan the trajectory
    (success_flag, trajectory_message, planning_time, error_code) = arm_group.plan()
    # Execute the trajectory and block while it's not finished
    arm_group.execute(trajectory_message, wait=True)


  def startExtruderCallback(self, extrusionAmount):
    pass


  def stopExtruderCallback(self):
    pass


  def moveAndExtrude(self, plan):
    self.startExtruderCallback(self.extrusionAmount)

    # Execute the trajectory and block while it's not finished
    self.arm_group.execute(plan, wait=True)

    self.stopExtruderCallback()


  def get_planning_frame(self):
    return self.tfPrefix + "/" + self.arm_group.get_planning_frame()


  def reach_cartesian_pose(self, pose):
    arm_group = self.arm_group
    current_pose = arm_group.get_current_pose()

    # Set the tolerance
    arm_group.set_goal_position_tolerance(self.tolerance)
    arm_group.allow_replanning(True)

    # set up constraints for motion while printing
    path_constraints = None

    if self.isPrinting:
      startPoint = geometry_msgs.msg.Point(x=current_pose.pose.position.x, y=current_pose.pose.position.y, z=current_pose.pose.position.z)
      endPoint = geometry_msgs.msg.Point(x=pose.pose.position.x, y=pose.pose.position.y, z=pose.pose.position.z)

      dx = endPoint.x - startPoint.x
      dy = endPoint.y - startPoint.y
      dz = endPoint.z - startPoint.z
      distance = math.sqrt(dx**2 + dy**2 + dz**2)

      # constraint box
      position_constraint = moveit_msgs.msg.PositionConstraint()
      position_constraint.header.frame_id = self.get_planning_frame()
      position_constraint.link_name = self.arm_group.get_end_effector_link()

      constraintBox = shape_msgs.msg.SolidPrimitive()
      constraintBox.type = shape_msgs.msg.SolidPrimitive.BOX
      constraintBox.dimensions = [self.filamentDiameter + self.tolerance, self.filamentDiameter + self.tolerance, distance + self.tolerance] # meters (length, width, height)

      position_constraint.constraint_region.primitives.append(constraintBox)

      # constraint box pose
      constraintBoxPose = geometry_msgs.msg.Pose()
      constraintBoxPose.position.x = (startPoint.x + endPoint.x) / 2
      constraintBoxPose.position.y = (startPoint.y + endPoint.y) / 2
      constraintBoxPose.position.z = (startPoint.z + endPoint.z) / 2

      yaw = math.atan2(dy, dx)
      pitch = math.atan2(math.sqrt(dx**2 + dy**2), dz)
      quaternion = tf.transformations.quaternion_from_euler(0, pitch, yaw)
      constraintBoxPose.orientation = geometry_msgs.msg.Quaternion(*quaternion)

      position_constraint.constraint_region.primitive_poses.append(constraintBoxPose)
      position_constraint.weight = 1.0

      # Create an orientation constraint to keep the end-effector pointing in the same direction
      orientation_constraint = moveit_msgs.msg.OrientationConstraint()
      orientation_constraint.header.frame_id = self.get_planning_frame()
      orientation_constraint.orientation = pose.pose.orientation
      orientation_constraint.link_name = self.arm_group.get_end_effector_link()
      orientation_constraint.absolute_x_axis_tolerance = 0.1
      orientation_constraint.absolute_y_axis_tolerance = 0.1
      orientation_constraint.absolute_z_axis_tolerance = 0.1
      orientation_constraint.weight = 1.0

      # Create a constraint message and add the position and orientation constraints to it
      path_constraints = moveit_msgs.msg.Constraints()
      path_constraints.position_constraints.append(position_constraint)
      path_constraints.orientation_constraints.append(orientation_constraint)

    if self.debug:
      startPoint = geometry_msgs.msg.Point(x=current_pose.pose.position.x, y=current_pose.pose.position.y, z=current_pose.pose.position.z)
      endPoint = geometry_msgs.msg.Point(x=pose.pose.position.x, y=pose.pose.position.y, z=pose.pose.position.z)

      dx = endPoint.x - startPoint.x
      dy = endPoint.y - startPoint.y
      dz = endPoint.z - startPoint.z
      distance = math.sqrt(dx**2 + dy**2 + dz**2)

      yaw = math.atan2(dy, dx)
      pitch = math.atan2(math.sqrt(dx**2 + dy**2), dz)
      quaternion = tf.transformations.quaternion_from_euler(0, pitch, yaw)

      marker = visualization_msgs.msg.Marker()
      marker.action = visualization_msgs.msg.Marker.ADD
      marker.id = self.markerCounter
      marker.header.frame_id = self.get_planning_frame()
      marker.type = visualization_msgs.msg.Marker.CUBE
      marker.scale.x = self.filamentDiameter + self.tolerance
      marker.scale.y = self.filamentDiameter + self.tolerance
      marker.scale.z = distance + self.tolerance
      marker.color.a = 0.5
      marker.color.r = 1 - self.filamentColor["r"]
      marker.color.g = 1 - self.filamentColor["g"]
      marker.color.b = 1 - self.filamentColor["b"]
      marker.pose.position.x = (startPoint.x + endPoint.x) / 2
      marker.pose.position.y = (startPoint.y + endPoint.y) / 2
      marker.pose.position.z = (startPoint.z + endPoint.z) / 2
      marker.pose.orientation = geometry_msgs.msg.Quaternion(*quaternion)
      self.marker_array.markers.append(marker)
      self.markerCounter += 1
      self.marker_pub.publish(self.marker_array)

    # planning using interpolation
    percentageOfPath = 0

    while percentageOfPath != 1: # when the fraction of the path is 1, the arm has fully reached the target
      waypoints = [current_pose.pose, pose.pose]
      plan, fraction = arm_group.compute_cartesian_path(waypoints, self.eef_step, self.jump_threshold, avoid_collisions=True, path_constraints=path_constraints)
      self.moveAndExtrude(plan)
      percentageOfPath = fraction
      current_pose = arm_group.get_current_pose()

    if self.debug:
      self.markerCounter -= 1

      # mark the marker as deleted
      marker = self.marker_array.markers[self.markerCounter]
      marker.action = visualization_msgs.msg.Marker.DELETE

      # display the new marker array
      self.marker_pub.publish(self.marker_array)

      # remove the marker from the array
      self.marker_array.markers.pop(self.markerCounter)


  def printMarkers(self, prevX=None, prevY=None, prevZ=None, nextX=None, nextY=None, nextZ=None):
    if not self.isPrinting:
      return

    actual_pose = self.arm_group.get_current_pose()

    if prevX is None:
      prevX=actual_pose.pose.position.x
    if prevY is None:
      prevY=actual_pose.pose.position.y
    if prevZ is None:
      prevZ=actual_pose.pose.position.z
    if nextX is None:
      nextX=actual_pose.pose.position.x
    if nextY is None:
      nextY=actual_pose.pose.position.y
    if nextZ is None:
      nextZ=actual_pose.pose.position.z

    startPoint = geometry_msgs.msg.Point(x=prevX, y=prevY, z=prevZ)
    endPoint = geometry_msgs.msg.Point(x=nextX, y=nextY, z=nextZ)

    marker = visualization_msgs.msg.Marker()
    marker.action = visualization_msgs.msg.Marker.ADD
    marker.id = self.markerCounter
    marker.header.frame_id = self.get_planning_frame()
    marker.type = visualization_msgs.msg.Marker.CYLINDER

    dx = endPoint.x - startPoint.x
    dy = endPoint.y - startPoint.y
    dz = endPoint.z - startPoint.z

    # dimensions of the cylinder
    marker.scale.x = self.filamentDiameter
    marker.scale.y = self.filamentDiameter
    marker.scale.z = math.sqrt(dx**2 + dy**2 + dz**2)

    # colors
    marker.color.a = self.filamentColor["a"]
    marker.color.r = self.filamentColor["r"]
    marker.color.g = self.filamentColor["g"]
    marker.color.b = self.filamentColor["b"]

    # position. it is between the points
    marker.pose.position.x = (startPoint.x + endPoint.x) / 2 + self.filamentXOffset
    marker.pose.position.y = (startPoint.y + endPoint.y) / 2 + self.filamentYOffset
    marker.pose.position.z = (startPoint.z + endPoint.z) / 2 + self.filamentZOffset

    # orientation. lay flat between the points
    yaw = math.atan2(dy, dx)
    pitch = math.atan2(math.sqrt(dx**2 + dy**2), dz)
    quaternion = tf.transformations.quaternion_from_euler(0, pitch, yaw)
    marker.pose.orientation = geometry_msgs.msg.Quaternion(*quaternion)

    # Publish marker
    self.marker_array.markers.append(marker)
    self.markerCounter += 1
    self.marker_pub.publish(self.marker_array)


  def goToCoordinate(self, homepose=None, abosolutePosition=True, x=None, y=None, z=None):
    """
    x, y, z are normalized
    """
    next_pose = self.arm_group.get_current_pose()

    if homepose is None:
      homepose = self.homePosition

    if abosolutePosition:
      if x is not None:
        armXMax = 1
        armXMin = homepose.pose.position.x
        armXStart = homepose.pose.position.x
        next_pose.pose.position.x = x * (armXMax - armXMin) + armXStart
      if y is not None:
        armYMax = 1
        armYMin = homepose.pose.position.x
        armYStart = homepose.pose.position.y
        next_pose.pose.position.y = y * (armYMax - armYMin) + armYStart
      if z is not None:
        armZMax = 1
        armZMin = homepose.pose.position.x
        armZStart = homepose.pose.position.z
        next_pose.pose.position.z = z * (armZMax - armZMin) + armZStart
    else:
      if x is None:
        x=0
      if y is None:
        y=0
      if z is None:
        z=0

      next_pose.pose.position.x += x
      next_pose.pose.position.y += y
      next_pose.pose.position.z += z

    self.reach_cartesian_pose(next_pose)


  def setFeedRate(self, f):
    if self.debug: # debug speed is the maximum speed
      self.arm_group.set_max_velocity_scaling_factor(1)
      return

    armMax = 1
    armMin = 0
    minVelocity = 0

    normalized = (f - minVelocity) / (self.maxVelocity - minVelocity)
    convertedToArm = normalized * (armMax - armMin) + armMin

    self.arm_group.set_max_velocity_scaling_factor(convertedToArm)


  def getCoordinate(self, command):
    x = None
    y = None
    z = None

    if 'x' in command:
        x = command['x']
    if 'y' in command:
        y = command['y']
    if 'z' in command:
        z = command['z']

    return (x, y, z)


  def xConversion(self, x):
    normalizedGcode = (x - self.xGcodeMin) / (self.xGcodeMax - self.xGcodeMin)
    convertToArmSpace = normalizedGcode * (self.xArmMax - self.xArmMin) + self.xArmMin

    return convertToArmSpace + self.xOffset


  def yConversion(self, y):
    normalizedGcode = (y - self.yGcodeMin) / (self.yGcodeMax - self.yGcodeMin)
    convertToArmSpace = normalizedGcode * (self.yArmMax - self.yArmMin) + self.yArmMin

    return convertToArmSpace + self.yOffset


  def zConversion(self, z):
    normalizedGcode = (z - self.zGcodeMin) / (self.zGcodeMax - self.zGcodeMin)
    convertToArmSpace = normalizedGcode * (self.zArmMax - self.zArmMin) + self.zArmMin

    return convertToArmSpace + self.zOffset


  def buildCoordinateString(self, commandNumber, robotID, coordinates):
    stateString = ""

    if 'x' in coordinates:
      stateString += "x" + str(coordinates["x"]) + " "

    if 'y' in coordinates:
      stateString += "y" + str(coordinates["y"]) + " "

    if 'z' in coordinates:
      stateString += "z" + str(coordinates["z"]) + " "

    return "a"+ str(commandNumber) + " r" + str(robotID) + " " + stateString[:-1]


  def updateLastState(self, command):
    if 'x' in command:
      self.state["x"] = command['x']

    if 'y' in command:
      self.state["y"] = command['y']

    if 'z' in command:
      self.state["z"] = command['z']


  def communicationHandler(self, command, data):
    # wait for communication hub subscriber
    while self.communication_hub.get_num_connections() == 0:
      rospy.sleep(1) # seconds

    isCommand = False

    if 'a' in command: # this should always be true unless something is not following the specification
      # print the command for debugging
      if self.debug:
        print(command)

      isCommand = True

      # if the command is for this robot, convert the parameters for this robot
      isThisRobot = self.robotID == int(command['r'])

      if isThisRobot:
        command = extractGCode.convertPositions([command], self.xConversion, self.yConversion, self.zConversion, True)
        command = command[0]

      command['a'] = int(command['a'])

      if command['a'] == 1: # start arm x
        if isThisRobot: # start this arm
          if not self.started:
            self.started = True
            gCodeExecution = threading.Thread(target=self.executeGCode)
            gCodeExecution.start()
        else:
          self.communication_hub.publish('a1 r' + str(command['r']))

      elif command['a'] == 2: # pause arm x
        if isThisRobot: # pause this arm
          self.paused = True
        else:
          self.communication_hub.publish('a2 r' + str(command['r']))

      elif command['a'] == 3: # move arm x to the senders position
        if isThisRobot: # check if this is the robot that is receiving the position
          self.isPrinting = False
          self.extrusionAmount = 0
          x, y, z = self.getCoordinate(command)
          x, y = y, x # the x and y axis are flipped in the g code
          self.goToCoordinate(x=x, y=y, z=z)
          self.updateLastState(command)
        else:
          self.communication_hub.publish(self.buildCoordinateString(command['a'], command['r'], self.state))

      elif command['a'] == 4: # clear chunk data of this arm
        self.gCodeChunk = ""

      elif command['a'] == 5: # save gcode chunk to this arm
        self.gCodeChunk = data

      elif command['a'] == 6: # resume arm x
        if isThisRobot: # resume this arm
          self.paused = False
        else:
          self.communication_hub.publish('a6 r' + str(command['r']))

      elif command['a'] == 7: # move arm x to the specified position
        if isThisRobot: # move this arm
          self.isPrinting = False
          self.extrusionAmount = 0
          x, y, z = self.getCoordinate(command)
          x, y = y, x # the x and y axis are flipped in the g code
          self.goToCoordinate(x=x, y=y, z=z)
          self.updateLastState(command)
        else:
          self.communication_hub.publish(self.buildCoordinateString(command['a'], command['r'], command))

    return isCommand


  def handleMessage(self, message):
    message = message.data

    index = message.find("\n")

    command = message
    data = ""

    if index != -1:
      command = message[:index]
      data = message[index:]

    command = extractGCode.extract(command)
    command = command[0]  # extractGCode.extract() returns a list, but we only have 1 element so we are going to use the element directly
    self.communicationHandler(command, data)


  def executeGCode(self):
    gCode = extractGCode.convertPositions(extractGCode.extract(self.gCodeChunk), self.xConversion, self.yConversion, self.zConversion)

    for command in gCode:
      # handle our custom commands first
      if not self.communicationHandler(command, ""):
        x, y, z = self.getCoordinate(command)

        if 'f' in command:
          self.setFeedRate(command['f'])

        if command['g'] == 1 and 'e' in command:
          if command['e'] > 0:
            self.isPrinting = True
          else:
            self.isPrinting = False
          self.extrusionAmount = command['e']

        self.updateLastState(command)

        prevPose = self.arm_group.get_current_pose().pose

        x, y = y, x # the x and y axis are flipped in the g code
        self.goToCoordinate(x=x, y=y, z=z)

        prevX = prevPose.position.x
        prevY = prevPose.position.y
        prevZ = prevPose.position.z

        self.printMarkers(prevX=prevX, prevY=prevY, prevZ=prevZ)

      # wait for resume command if the arm is paused
      while self.paused:
        rospy.sleep(1) # seconds

    self.started = False


  def start(self):
    rospy.spin()


  def attach_extruder(self, extruder_length = 0.025, extruder_width = 0.025, extruder_height = 0.1):
    # create a box in the planning scene
    box_pose = self.arm_group.get_current_pose()
    box_pose.pose.orientation.x = 0
    box_pose.pose.orientation.y = 0
    box_pose.pose.orientation.z = 0
    box_pose.pose.orientation.w = 1.0
    box_pose.pose.position.z -= extruder_height / 2

    self.scene.add_box(self.extruder_name, box_pose, size = (extruder_length, extruder_width, extruder_height))
    touch_links = self.robot.get_link_names()
    self.scene.attach_box(self.arm_group.get_end_effector_link(), self.extruder_name, touch_links=touch_links)


  def attach_ground(self, length = 8, width = 8, height = 0.001):
    # create a box in the planning scene
    box_pose = geometry_msgs.msg.PoseStamped()
    box_pose.header.frame_id = self.arm_group.get_planning_frame()
    box_pose.pose.orientation.w = 1.0
    box_pose.pose.position.z = -height / 2 - 0.125
    ground_name = "ground"

    self.scene.add_box(ground_name, box_pose, size = (length, width, height))
