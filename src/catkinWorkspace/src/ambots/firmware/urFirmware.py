#!/usr/bin/env python3

# for opening files
import sys
import os
fileDirectory = os.path.realpath(__file__)
fileDirectory = os.path.dirname(fileDirectory)
sys.path.insert(0, fileDirectory)

# firmware
import firmware


class URFirmware(firmware.Firmware):
  def __init__(self, debug = False):
    super().__init__(
      name= "ur",
      arm_group_name= "manipulator",
      tfPrefix= "ur",
      xGcodeMax= 1200,
      yGcodeMax= 0,
      zGcodeMax= 1200,
      xGcodeMin= 0,
      yGcodeMin= 1200,
      xArmMax = 2,
      yArmMax= 1,
      zArmMax= -5,
      xArmMin = 6,
      yArmMin= 5,
      zArmMin= -1,
      zOffset= 0.2475, # ex: the print head height, up and down
      xOffset= -3.415, # align print area, side to side
      yOffset= -0.55, # align print area, forwards and backwards
      filamentDiameter= 0.004,
      maxVelocity= 1000,
      filamentColor= {
        "r":0.0,
        "g":1.0,
        "b":0.0,
        "a":1.0
      },
      debug= debug,
      topic= "/ambots/ur_topic",
      robotID= 1,
      eef_step= 1
    )


def main():
  urArm = URFirmware(debug=True)

  urArm.start()


if __name__ == '__main__':
  main()
