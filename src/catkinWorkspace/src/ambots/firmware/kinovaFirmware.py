#!/usr/bin/env python3

# for opening files
import sys
import os
fileDirectory = os.path.realpath(__file__)
fileDirectory = os.path.dirname(fileDirectory)
sys.path.insert(0, fileDirectory)

# firmware
import firmware
import rospy


class KinovaFirmware(firmware.Firmware):
  def __init__(self, degrees_of_freedom = 6, debug = False):
    super().__init__(
      name= "kinova",
      arm_group_name= "arm",
      tfPrefix= "kinova",
      xGcodeMax= 0,
      yGcodeMax= 891,
      zGcodeMax= 891,
      xGcodeMin= 891,
      xArmMin = 0,
      yArmMin= 0,
      zArmMin= 0,
      zOffset= -0.5, # ex: the print head height, up and down
      xOffset= -0.6, # move the print area closer to the arm, side to side
      yOffset= -0.1, # move the print area closer to the arm, forwards and backwards
      filamentDiameter= 0.004,
      maxVelocity= 500,
      filamentColor= {
        "r":1.0,
        "g":0.0,
        "b":0.0,
        "a":1.0
      },
      debug= debug,
      topic= "/ambots/kinova_topic",
      robotID= 0,
      eef_step= 0.05
    )

    self.degrees_of_freedom = rospy.get_param(rospy.get_namespace() + "degrees_of_freedom", degrees_of_freedom)


def main():
  kinovaArm = KinovaFirmware(debug=True)

  kinovaArm.start()


if __name__ == '__main__':
  main()
