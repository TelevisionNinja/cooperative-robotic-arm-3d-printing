#!/usr/bin/env python3

# for opening files
import sys
import os
fileDirectory = os.path.realpath(__file__)
fileDirectory = os.path.dirname(fileDirectory)
fileDirectory = os.path.dirname(fileDirectory) # parent directory
sys.path.insert(0, fileDirectory)

# gcode
import extractGCode

# communication hub
import rospy
from std_msgs.msg import String


class CommunicationHub:
    def __init__(self,
            communication_hub_topic_name = '/ambots/communication_hub_topic',
            debug = False
        ):

        rospy.init_node('communicationHub', anonymous=True)
        self.publishers = {}
        self.msg_type = String
        self.communication_hub_topic_name = communication_hub_topic_name
        self.communication_hub_topic = rospy.Subscriber(self.communication_hub_topic_name, self.msg_type, self.ack)

        self.debug = debug


    def startArm(self, robotID):
        self.sendData('a1 r' + str(robotID), int(robotID))


    def resumeArm(self, robotID):
        self.sendData('a6 r' + str(robotID), int(robotID))


    def pauseArm(self, robotID):
        self.sendData('a2 r' + str(robotID), int(robotID))


    def sendStateData(self, robotID, data):
        self.sendData(data, int(robotID))


    def clearChunkData(self, robotID):
        self.sendData('a4 r' + str(robotID), int(robotID))


    def sendChunk(self, robotID, chunk):
        self.sendData(chunk, int(robotID))


    def sendCoordinates(self, robotID, data):
        self.sendData(data, int(robotID))


    def ack(self, data):
        data = data.data

        dataMap = extractGCode.extract(data)
        dataMap = dataMap[0] # extractGCode.extract() returns a list, but we only have 1 element so we are going to use the element directly

        if self.debug:
            print("received:", dataMap)

        if 'a' in dataMap: # this should always be true unless something is not following the specification
            if dataMap['a'] == 1: # start arm x
                self.startArm(dataMap['r'])
            elif dataMap['a'] == 2: # pause arm x
                self.pauseArm(dataMap['r'])
            elif dataMap['a'] == 3: # send state data to arm x
                self.sendStateData(dataMap['r'], data)
            elif dataMap['a'] == 4: # clear chunk data of arm x
                self.clearChunkData(dataMap['r'])
            elif dataMap['a'] == 5: # send gcode chunk to arm x
                self.sendChunk(dataMap['r'], data)
            elif dataMap['a'] == 6: # resume arm x
                self.resumeArm(dataMap['r'])
            elif dataMap['a'] == 7: # send coordinates to arm x
                self.sendCoordinates(dataMap['r'], data)


    def add_robot(self, robot_name, topic_name):
        self.publishers[robot_name] = rospy.Publisher(topic_name, self.msg_type, queue_size=1000)

        # wait for subscriber
        while self.publishers[robot_name].get_num_connections() == 0:
            rospy.sleep(1) # seconds

        return True


    def publish(self, msg, robotID):
        self.publishers[robotID].publish(msg)


    def sendData(self, command, robotID):
        self.publish(command, robotID)

        if self.debug:
            print("send:", command)


    def start(self):
        rospy.spin()


def main():
    communicationHub = CommunicationHub(debug=True)
    communicationHub.add_robot(0, '/ambots/kinova_topic')
    communicationHub.add_robot(1, '/ambots/ur_topic')
    communicationHub.start()


if __name__ == '__main__':
    main()
