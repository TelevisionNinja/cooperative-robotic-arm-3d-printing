#!/usr/bin/env python3

# for extracting gcode
import re

# for opening files
import os


extractGCodeRegex = re.compile(r"(^(G|A)\d+( \w-?\d+(\.\d+)?)*$)", re.IGNORECASE | re.MULTILINE)
parameterSet = {'g', 'x', 'y', 'z', 'e', 'f', 'a', 'r'}

def extract(gCode):
    """
    example of a return value:

    using the positionArry shown above it looks like this:
    [
        {
            g: 1,
            x: 2,
            y: 3,
            z: 4,
            e: 5,
            f: 6
        },
        {
            g: 1,
            x: 2,
            y: 3,
            z: 4,
            e: 5,
            f: 6
        },
        ......
    ]
    """

    matches = extractGCodeRegex.findall(gCode.lower())
    commands = []

    for match in matches:
        matchParameters = match[0].split()
        command = {}

        for parameter in matchParameters:
            if parameter[:1] in parameterSet:
                command[parameter[:1]] = float(parameter[1:])

        commands.append(command)

    return commands


def extractFile(fileName):
    coordinates = []

    if os.path.isfile(fileName):
        with open(fileName) as file:
            coordinates = extract(file.read())

    return coordinates


def extractRaw(gCode):
    matches = extractGCodeRegex.findall(gCode.lower())
    commands = []

    for match in matches:
        commands.append(match[0])

    return commands


def extractFileRaw(fileName):
    coordinates = []

    if os.path.isfile(fileName):
        with open(fileName) as file:
            coordinates = extractRaw(file.read())

    return coordinates


def defaultXConversion(x):
    armMax = 1
    armMin = 0
    gcodeMax = 300
    gcodeMin = 0
    offset = 0

    normalizedGcode = (x - gcodeMin) / (gcodeMax - gcodeMin)
    convertToArmSpace = normalizedGcode * (armMax - armMin) + armMin

    return convertToArmSpace + offset


def defaultYConversion(y):
    armMax = 1
    armMin = 0
    gcodeMax = 300
    gcodeMin = 0
    offset = 0

    normalizedGcode = (y - gcodeMin) / (gcodeMax - gcodeMin)
    convertToArmSpace = normalizedGcode * (armMax - armMin) + armMin

    return convertToArmSpace + offset


def defaultZConversion(z):
    armMax = 1
    armMin = 0
    gcodeMax = 300
    gcodeMin = 0
    offset = -0.5 # ex: the print head height

    normalizedGcode = (z - gcodeMin) / (gcodeMax - gcodeMin)
    convertToArmSpace = normalizedGcode * (armMax - armMin) + armMin

    return convertToArmSpace + offset


def convertPositions(arr, xConvert = defaultXConversion, yConvert = defaultYConversion, zConvert = defaultZConversion, convertCustomCommands = False):
    """
    For every position, this function scales the coordinates down to get a value between 0 and 1, which is what is needed by the arm.

    xConvert, yConvert, zConvert are functions that convert the specified axis. They accept a floating point number and return a floating point number
    """

    for curr in arr:
        if convertCustomCommands or not ('a' in curr):
            if 'x' in curr:
                curr['x'] = xConvert(curr['x'])
            if 'y' in curr:
                curr['y'] = yConvert(curr['y'])
            if 'z' in curr:
                curr['z'] = zConvert(curr['z'])

    return arr


def getCoordinate(command):
    x = None
    y = None
    z = None

    if 'x' in command:
        x = command['x']
    if 'y' in command:
        y = command['y']
    if 'z' in command:
        z = command['z']

    return (x, y, z)
