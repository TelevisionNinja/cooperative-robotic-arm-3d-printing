G1 E-2.1 F2700
;WIPE_START
G1 F24000
G1 X403.496 Y491.101 E-.75737
G1 X404.41 Y491.101 E-.09763
;WIPE_END
G1 E-.045 F2700
G1 Z4.2 F30000
G1 Z7
;AFTER_LAYER_CHANGE
;7
G1 Z7.2
G1 X404.41 Y501.642
G1 Z7
G1 E3 F1200
;TYPE:Skirt/Brim
;WIDTH:4
G1 F89.387
G1 X395.5 Y501.642 E7.13882
G1 X393.328 Y501.245 E1.76876
G1 X391.437 Y500.106 E1.76876
G1 X390.071 Y498.372 E1.76876
G1 X389.358 Y495.5 E2.37082
G1 X389.358 Y395.5 E80.12324
G1 X389.755 Y393.328 E1.76876
M73 P20 R201
G1 X390.894 Y391.437 E1.76876
G1 X392.628 Y390.071 E1.76876
G1 X395.5 Y389.358 E2.37082
G1 X495.5 Y389.358 E80.12324
G1 X497.672 Y389.755 E1.76876
M73 P21 R199
G1 X499.563 Y390.894 E1.76876
G1 X500.929 Y392.628 E1.76876
G1 X501.642 Y395.5 E2.37082
G1 X501.642 Y495.5 E80.12324
G1 X501.245 Y497.672 E1.76876
M73 P21 R198
G1 X500.106 Y499.563 E1.76876
G1 X498.372 Y500.929 E1.76876
G1 X495.5 Y501.642 E2.37082
G1 X405.01 Y501.642 E72.50369
; printing object 20mm_cube.stl id:0 copy 0
G1 E-2.1 F2700
;WIPE_START
M73 P22 R197
G1 F24000
G1 X397.01 Y501.642 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z7.2 F30000
G1 X493.5 Y493.5
G1 Z7
G1 E3 F1200
M204 P800
;TYPE:External perimeter
;WIDTH:4
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P22 R196
G1 X493.5 Y397.5 E76.9183
M73 P22 R195
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
M106 S170.85
;LAYER_CHANGE
;Z:10
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;10


M73 P23 R194
G1 E-2.1 F2700
;WIPE_START
M73 P23 R193
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z7.2 F30000
G1 Z10
;AFTER_LAYER_CHANGE
;10
; printing object 20mm_cube.stl id:0 copy 0
G1 Z10.2
G1 X493.5 Y493.5
G1 Z10
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P24 R192
G1 X493.5 Y397.5 E76.9183
M73 P24 R191
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
M106 S255
;LAYER_CHANGE
;Z:13
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;13


M73 P25 R190
G1 E-2.1 F2700
;WIPE_START
M73 P25 R188
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z10.2 F30000
G1 Z13
;AFTER_LAYER_CHANGE
;13
; printing object 20mm_cube.stl id:0 copy 0
G1 Z13.2
G1 X493.5 Y493.5
G1 Z13
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P25 R187
G1 X493.5 Y397.5 E76.9183
M73 P26 R186
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:16
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;16


M73 P26 R185
G1 E-2.1 F2700
;WIPE_START
M73 P27 R184
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z13.2 F30000
G1 Z16
;AFTER_LAYER_CHANGE
;16
; printing object 20mm_cube.stl id:0 copy 0
G1 Z16.2
G1 X493.5 Y493.5
G1 Z16
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P27 R183
G1 X493.5 Y397.5 E76.9183
M73 P28 R182
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:19
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;19


M73 P28 R181
G1 E-2.1 F2700
;WIPE_START
M73 P28 R180
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z16.2 F30000
G1 Z19
;AFTER_LAYER_CHANGE
;19
; printing object 20mm_cube.stl id:0 copy 0
G1 Z19.2
G1 X493.5 Y493.5
G1 Z19
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P29 R179
G1 X493.5 Y397.5 E76.9183
M73 P29 R178
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:22
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;22


M73 P30 R177
G1 E-2.1 F2700
;WIPE_START
M73 P30 R176
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z19.2 F30000
G1 Z22
;AFTER_LAYER_CHANGE
;22
; printing object 20mm_cube.stl id:0 copy 0
G1 Z22.2
G1 X493.5 Y493.5
G1 Z22
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P31 R174
G1 X493.5 Y397.5 E76.9183
M73 P31 R173
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:25
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;25


M73 P31 R172
G1 E-2.1 F2700
;WIPE_START
M73 P32 R171
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z22.2 F30000
G1 Z25
;AFTER_LAYER_CHANGE
;25
; printing object 20mm_cube.stl id:0 copy 0
G1 Z25.2
G1 X493.5 Y493.5
G1 Z25
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P32 R170
G1 X493.5 Y397.5 E76.9183
M73 P33 R169
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:28
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;28


M73 P33 R168
G1 E-2.1 F2700
;WIPE_START
M73 P33 R167
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
M73 P34 R167
G1 Z25.2 F30000
G1 Z28
;AFTER_LAYER_CHANGE
;28
; printing object 20mm_cube.stl id:0 copy 0
G1 Z28.2
G1 X493.5 Y493.5
G1 Z28
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P34 R166
G1 X493.5 Y397.5 E76.9183
M73 P34 R165
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:31
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;31


M73 P35 R164
G1 E-2.1 F2700
;WIPE_START
M73 P35 R163
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z28.2 F30000
G1 Z31
;AFTER_LAYER_CHANGE
;31
; printing object 20mm_cube.stl id:0 copy 0
G1 Z31.2
G1 X493.5 Y493.5
G1 Z31
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P36 R162
G1 X493.5 Y397.5 E76.9183
M73 P36 R160
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:34
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;34


M73 P36 R159
G1 E-2.1 F2700
;WIPE_START
M73 P37 R158
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z31.2 F30000
G1 Z34
;AFTER_LAYER_CHANGE
;34
; printing object 20mm_cube.stl id:0 copy 0
G1 Z34.2
G1 X493.5 Y493.5
G1 Z34
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P37 R157
G1 X493.5 Y397.5 E76.9183
M73 P38 R156
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:37
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;37

M73 P38 R155
G1 E-2.1 F2700
;WIPE_START
M73 P39 R154
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z34.2 F30000
G1 Z37
;AFTER_LAYER_CHANGE
;37
; printing object 20mm_cube.stl id:0 copy 0
G1 Z37.2
G1 X493.5 Y493.5
G1 Z37
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P39 R153
G1 X493.5 Y397.5 E76.9183
M73 P39 R152
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:40
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;40


M73 P40 R151
G1 E-2.1 F2700
;WIPE_START
M73 P40 R150
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z37.2 F30000
G1 Z40
;AFTER_LAYER_CHANGE
;40
; printing object 20mm_cube.stl id:0 copy 0
G1 Z40.2
G1 X493.5 Y493.5
G1 Z40
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P41 R149
G1 X493.5 Y397.5 E76.9183
M73 P41 R148
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:43
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;43


M73 P42 R147
G1 E-2.1 F2700
;WIPE_START
M73 P42 R145
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z40.2 F30000
G1 Z43
;AFTER_LAYER_CHANGE
;43
; printing object 20mm_cube.stl id:0 copy 0
G1 Z43.2
G1 X493.5 Y493.5
G1 Z43
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P42 R144
G1 X493.5 Y397.5 E76.9183
M73 P43 R143
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:46
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;46


M73 P43 R142
G1 E-2.1 F2700
;WIPE_START
M73 P44 R141
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z43.2 F30000
G1 Z46
;AFTER_LAYER_CHANGE
;46
; printing object 20mm_cube.stl id:0 copy 0
G1 Z46.2
G1 X493.5 Y493.5
G1 Z46
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P44 R140
G1 X493.5 Y397.5 E76.9183
M73 P45 R139
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:49
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;49


M73 P45 R138
G1 E-2.1 F2700
;WIPE_START
M73 P45 R137
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z46.2 F30000
G1 Z49
;AFTER_LAYER_CHANGE
;49
; printing object 20mm_cube.stl id:0 copy 0
G1 Z49.2
G1 X493.5 Y493.5
G1 Z49
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P46 R136
G1 X493.5 Y397.5 E76.9183
M73 P46 R135
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:52
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;52


M73 P47 R134
G1 E-2.1 F2700
;WIPE_START
M73 P47 R133
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z49.2 F30000
G1 Z52
;AFTER_LAYER_CHANGE
;52
; printing object 20mm_cube.stl id:0 copy 0
G1 Z52.2
G1 X493.5 Y493.5
G1 Z52
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P47 R131
G1 X493.5 Y397.5 E76.9183
M73 P48 R130
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:55
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;55


M73 P48 R129
G1 E-2.1 F2700
;WIPE_START
M73 P49 R128
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z52.2 F30000
G1 Z55
;AFTER_LAYER_CHANGE
;55
; printing object 20mm_cube.stl id:0 copy 0
G1 Z55.2
G1 X493.5 Y493.5
G1 Z55
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P49 R127
G1 X493.5 Y397.5 E76.9183
M73 P50 R126
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:58
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;58


M73 P50 R125
G1 E-2.1 F2700
;WIPE_START
M73 P50 R124
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z55.2 F30000
G1 Z58
;AFTER_LAYER_CHANGE
;58
; printing object 20mm_cube.stl id:0 copy 0
G1 Z58.2
G1 X493.5 Y493.5
G1 Z58
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P51 R123
G1 X493.5 Y397.5 E76.9183
M73 P51 R122
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:61
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;61


M73 P52 R121
G1 E-2.1 F2700
;WIPE_START
M73 P52 R120
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z58.2 F30000
G1 Z61
;AFTER_LAYER_CHANGE
;61
; printing object 20mm_cube.stl id:0 copy 0
G1 Z61.2
G1 X493.5 Y493.5
G1 Z61
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P53 R119
G1 X493.5 Y397.5 E76.9183
M73 P53 R117
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:64
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;64


M73 P53 R116
G1 E-2.1 F2700
;WIPE_START
M73 P54 R115
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z61.2 F30000
G1 Z64
;AFTER_LAYER_CHANGE
;64
; printing object 20mm_cube.stl id:0 copy 0
G1 Z64.2
G1 X493.5 Y493.5
G1 Z64
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P54 R114
G1 X493.5 Y397.5 E76.9183
M73 P55 R113
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:67
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;67


M73 P55 R112
G1 E-2.1 F2700
;WIPE_START
M73 P56 R111
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z64.2 F30000
G1 Z67
;AFTER_LAYER_CHANGE
;67
; printing object 20mm_cube.stl id:0 copy 0
G1 Z67.2
G1 X493.5 Y493.5
G1 Z67
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P56 R110
G1 X493.5 Y397.5 E76.9183
M73 P56 R109
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:70
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;70


M73 P57 R108
G1 E-2.1 F2700
;WIPE_START
M73 P57 R107
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z67.2 F30000
G1 Z70
;AFTER_LAYER_CHANGE
;70
; printing object 20mm_cube.stl id:0 copy 0
G1 Z70.2
G1 X493.5 Y493.5
G1 Z70
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P58 R106
G1 X493.5 Y397.5 E76.9183
M73 P58 R105
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:73
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;73

M73 P58 R104
G1 E-2.1 F2700
;WIPE_START
M73 P59 R102
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z70.2 F30000
G1 Z73
;AFTER_LAYER_CHANGE
;73
; printing object 20mm_cube.stl id:0 copy 0
G1 Z73.2
G1 X493.5 Y493.5
G1 Z73
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P59 R101
G1 X493.5 Y397.5 E76.9183
M73 P60 R100
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:76
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;76


M73 P60 R99
G1 E-2.1 F2700
;WIPE_START
M73 P61 R98
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z73.2 F30000
G1 Z76
;AFTER_LAYER_CHANGE
;76
; printing object 20mm_cube.stl id:0 copy 0
G1 Z76.2
G1 X493.5 Y493.5
G1 Z76
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P61 R97
G1 X493.5 Y397.5 E76.9183
M73 P61 R96
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:79
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;79


M73 P62 R95
G1 E-2.1 F2700
;WIPE_START
M73 P62 R94
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z76.2 F30000
G1 Z79
;AFTER_LAYER_CHANGE
;79
; printing object 20mm_cube.stl id:0 copy 0
G1 Z79.2
G1 X493.5 Y493.5
G1 Z79
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P63 R93
G1 X493.5 Y397.5 E76.9183
M73 P63 R92
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:82
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;82


M73 P64 R91
G1 E-2.1 F2700
;WIPE_START
M73 P64 R90
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z79.2 F30000
G1 Z82
;AFTER_LAYER_CHANGE
;82
; printing object 20mm_cube.stl id:0 copy 0
G1 Z82.2
G1 X493.5 Y493.5
G1 Z82
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P64 R88
G1 X493.5 Y397.5 E76.9183
M73 P65 R87
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:85
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;85


M73 P65 R86
G1 E-2.1 F2700
;WIPE_START
M73 P66 R85
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z82.2 F30000
G1 Z85
;AFTER_LAYER_CHANGE
;85
; printing object 20mm_cube.stl id:0 copy 0
G1 Z85.2
G1 X493.5 Y493.5
G1 Z85
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P66 R84
G1 X493.5 Y397.5 E76.9183
M73 P67 R83
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:88
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;88


M73 P67 R82
G1 E-2.1 F2700
;WIPE_START
M73 P67 R81
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z85.2 F30000
G1 Z88
;AFTER_LAYER_CHANGE
;88
; printing object 20mm_cube.stl id:0 copy 0
G1 Z88.2
G1 X493.5 Y493.5
G1 Z88
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P68 R80
G1 X493.5 Y397.5 E76.9183
M73 P68 R79
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:91
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;91


M73 P69 R78
G1 E-2.1 F2700
;WIPE_START
M73 P69 R77
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z88.2 F30000
G1 Z91
;AFTER_LAYER_CHANGE
;91
; printing object 20mm_cube.stl id:0 copy 0
G1 Z91.2
G1 X493.5 Y493.5
G1 Z91
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P70 R76
G1 X493.5 Y397.5 E76.9183
M73 P70 R75
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:94
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;94


M73 P70 R73
G1 E-2.1 F2700
;WIPE_START
M73 P71 R72
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z91.2 F30000
G1 Z94
;AFTER_LAYER_CHANGE
;94
; printing object 20mm_cube.stl id:0 copy 0
G1 Z94.2
G1 X493.5 Y493.5
G1 Z94
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P71 R71
G1 X493.5 Y397.5 E76.9183
M73 P72 R70
G1 X493.5 Y492.9 E76.43756
M204 P1000
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:97
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;97


M73 P72 R69
G1 E-2.1 F2700
;WIPE_START
M73 P72 R68
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z94.2 F30000
G1 Z97
;AFTER_LAYER_CHANGE
;97
; printing object 20mm_cube.stl id:0 copy 0
G1 Z97.2
G1 X493.5 Y493.5
G1 Z97
G1 E3 F1200
M204 P800
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P73 R67
G1 X493.5 Y397.5 E76.9183
M73 P73 R66
G1 X493.5 Y492.9 E76.43756
M204 P1000
M73 P74 R65
G1 E-2.1 F2700
;WIPE_START
M73 P74 R64
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z97.2 F30000
G1 X492.637 Y484.669
G1 Z97
G1 E3 F1200
;TYPE:Bridge infill
;WIDTH:3.88723
;HEIGHT:3.79473
G1 F75.835
G1 X486.399 Y490.906 E8.33093
G1 X480.831 Y490.906 E5.25857
G1 X490.906 Y480.831 E13.45691
G1 X490.906 Y475.263 E5.25856
M73 P74 R63
G1 X475.263 Y490.906 E20.89364
G1 X469.695 Y490.906 E5.25857
G1 X490.906 Y469.695 E28.33038
G1 X490.906 Y464.127 E5.25856
M73 P75 R63
G1 X464.127 Y490.906 E35.76711
M73 P75 R62
G1 X458.558 Y490.906 E5.25857
G1 X490.906 Y458.558 E43.20385
G1 X490.906 Y452.99 E5.25856
M73 P75 R61
G1 X452.99 Y490.906 E50.64058
G1 X447.422 Y490.906 E5.25857
G1 X490.906 Y447.422 E58.07732
M73 P75 R60
G1 X490.906 Y441.854 E5.25856
M73 P76 R60
G1 X441.854 Y490.906 E65.51405
G1 X436.286 Y490.906 E5.25857
M73 P76 R59
G1 X490.906 Y436.286 E72.95078
G1 X490.906 Y430.718 E5.25856
M73 P77 R58
G1 X430.718 Y490.906 E80.38752
M73 P77 R57
G1 X425.15 Y490.906 E5.25857
M73 P77 R56
G1 X490.906 Y425.15 E87.82425
G1 X490.906 Y419.582 E5.25856
M73 P78 R55
G1 X419.582 Y490.906 E95.26099
G1 X414.014 Y490.906 E5.25857
M73 P78 R54
G1 X490.906 Y414.014 E102.69772
G1 X490.906 Y408.446 E5.25856
M73 P79 R52
G1 X408.446 Y490.906 E110.13445
G1 X402.878 Y490.906 E5.25857
M73 P79 R51
G1 X490.906 Y402.878 E117.57119
M73 P79 R50
G1 X490.906 Y400.094 E2.62925
M73 P80 R49
G1 X488.122 Y400.094 E2.62931
G1 X400.094 Y488.122 E117.57106
G1 X400.094 Y482.554 E5.25856
M73 P81 R47
G1 X482.554 Y400.094 E110.13433
G1 X476.986 Y400.094 E5.25857
M73 P81 R45
G1 X400.094 Y476.986 E102.69759
G1 X400.094 Y471.418 E5.25856
M73 P82 R44
G1 X471.418 Y400.094 E95.26086
G1 X465.85 Y400.094 E5.25857
M73 P83 R43
G1 X400.094 Y465.85 E87.82412
G1 X400.094 Y460.282 E5.25856
M73 P83 R41
G1 X460.282 Y400.094 E80.38739
G1 X454.714 Y400.094 E5.25857
M73 P84 R40
G1 X400.094 Y454.714 E72.95065
G1 X400.094 Y449.146 E5.25856
M73 P84 R39
G1 X449.146 Y400.094 E65.51392
G1 X443.578 Y400.094 E5.25857
M73 P84 R38
G1 X400.094 Y443.578 E58.07719
G1 X400.094 Y438.01 E5.25856
M73 P85 R37
G1 X438.01 Y400.094 E50.64045
G1 X432.441 Y400.094 E5.25857
M73 P85 R36
G1 X400.094 Y432.441 E43.20372
G1 X400.094 Y426.873 E5.25856
G1 X426.873 Y400.094 E35.76698
G1 X421.305 Y400.094 E5.25857
M73 P85 R35
G1 X400.094 Y421.305 E28.33025
M73 P86 R35
G1 X400.094 Y415.737 E5.25856
G1 X415.737 Y400.094 E20.89351
G1 X410.169 Y400.094 E5.25857
M73 P86 R34
G1 X400.094 Y410.169 E13.45678
G1 X400.094 Y404.601 E5.25856
G1 X406.331 Y398.363 E8.33081
; stop printing object 20mm_cube.stl id:0 copy 0
;LAYER_CHANGE
;Z:100
;HEIGHT:3
;BEFORE_LAYER_CHANGE
G92 E0.0
;100


G1 E-2.1 F2700
;WIPE_START
G1 F24000
G1 X400.674 Y404.02 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z97.2 F30000
G1 Z100
;AFTER_LAYER_CHANGE
;100
; printing object 20mm_cube.stl id:0 copy 0
G1 Z100.2
G1 X493.5 Y493.5
G1 Z100
G1 E3 F1200
M204 P800
;TYPE:External perimeter
;WIDTH:4
G1 F89.387
G1 X397.5 Y493.5 E76.9183
G1 X397.5 Y397.5 E76.9183
M73 P86 R33
G1 X493.5 Y397.5 E76.9183
M73 P87 R32
G1 X493.5 Y492.9 E76.43756
M204 P1000
M73 P87 R31
G1 E-2.1 F2700
;WIPE_START
M73 P88 R30
G1 F24000
G1 X485.5 Y492.95 E-.855
;WIPE_END
G1 E-.045 F2700
G1 Z100.2 F30000
G1 X485.715 Y398.339
G1 Z100
M73 P88 R29
G1 E3 F1200
;TYPE:Top solid infill
;WIDTH:4.07627
G1 F87.401
G1 X491.151 Y403.775 E6.29901
G1 X491.151 Y408.629 E3.97775
G1 X482.371 Y399.849 E10.17419
G1 X477.517 Y399.849 E3.97775
G1 X491.151 Y413.483 E15.79957
G1 X491.151 Y418.337 E3.97775
G1 X472.663 Y399.849 E21.42496
G1 X467.809 Y399.849 E3.97775
G1 X491.151 Y423.191 E27.05035
G1 X491.151 Y428.046 E3.97775
M73 P88 R28
G1 X462.954 Y399.849 E32.67574
G1 X458.1 Y399.849 E3.97775
G1 X491.151 Y432.9 E38.30113
G1 X491.151 Y437.754 E3.97775
M73 P89 R27
G1 X453.246 Y399.849 E43.92652
G1 X448.392 Y399.849 E3.97775
M73 P89 R26
G1 X491.151 Y442.608 E49.55191
G1 X491.151 Y447.463 E3.97775
G1 X443.537 Y399.849 E55.1773
G1 X438.683 Y399.849 E3.97775
M73 P90 R25
G1 X491.151 Y452.317 E60.80269
G1 X491.151 Y457.171 E3.97775
M73 P90 R24
G1 X433.829 Y399.849 E66.42808
G1 X428.975 Y399.849 E3.97775
M73 P90 R23
G1 X491.151 Y462.025 E72.05347
G1 X491.151 Y466.88 E3.97775
M73 P91 R22
G1 X424.12 Y399.849 E77.67886
G1 X419.266 Y399.849 E3.97775
M73 P91 R21
G1 X491.151 Y471.734 E83.30425
G1 X491.151 Y476.588 E3.97775
M73 P92 R19
G1 X414.412 Y399.849 E88.92964
G1 X409.558 Y399.849 E3.97775
M73 P92 R18
G1 X491.151 Y481.442 E94.55503
G1 X491.151 Y486.297 E3.97775
M73 P93 R17
G1 X404.703 Y399.849 E100.18041
G1 X399.849 Y399.849 E3.97769
M73 P93 R15
G1 X491.151 Y491.151 E105.80564
G1 X486.296 Y491.151 E3.97775
M73 P94 R14
G1 X399.849 Y404.704 E100.18025
G1 X399.849 Y409.558 E3.97775
M73 P94 R12
G1 X481.442 Y491.151 E94.55486
G1 X476.588 Y491.151 E3.97775
M73 P95 R11
G1 X399.849 Y414.412 E88.92948
G1 X399.849 Y419.266 E3.97775
M73 P95 R10
G1 X471.734 Y491.151 E83.30409
M73 P96 R10
G1 X466.879 Y491.151 E3.97775
M73 P96 R8
G1 X399.849 Y424.121 E77.6787
G1 X399.849 Y428.975 E3.97775
M73 P96 R7
G1 X462.025 Y491.151 E72.05331
G1 X457.171 Y491.151 E3.97775
M73 P97 R6
G1 X399.849 Y433.829 E66.42792
G1 X399.849 Y438.683 E3.97775
M73 P97 R5
G1 X452.317 Y491.151 E60.80253
G1 X447.463 Y491.151 E3.97775
M73 P98 R4
G1 X399.849 Y443.537 E55.17714
G1 X399.849 Y448.392 E3.97775
G1 X442.608 Y491.151 E49.55175
G1 X437.754 Y491.151 E3.97775
M73 P98 R3
G1 X399.849 Y453.246 E43.92636
G1 X399.849 Y458.1 E3.97775
M73 P98 R2
G1 X432.9 Y491.151 E38.30097
G1 X428.046 Y491.151 E3.97775
M73 P99 R2
G1 X399.849 Y462.954 E32.67558
G1 X399.849 Y467.809 E3.97775
M73 P99 R1
G1 X423.191 Y491.151 E27.05019
G1 X418.337 Y491.151 E3.97775
G1 X399.849 Y472.663 E21.4248
G1 X399.849 Y477.517 E3.97775
M73 P99 R0
G1 X413.483 Y491.151 E15.79941
G1 X408.629 Y491.151 E3.97775
G1 X399.849 Y482.371 E10.17403
G1 X399.849 Y487.226 E3.97775
G1 X405.285 Y492.661 E6.29885
; stop printing object 20mm_cube.stl id:0 copy 0
G1 E-2.1 F2700
;WIPE_START
G1 F24000;_WIPE
G1 X399.849 Y487.226 E-.82152
G1 F24000;_WIPE
G1 X399.849 Y486.912 E-.03348
;WIPE_END
G1 E-.045 F2700
G1 Z100.2 F30000
M107
;TYPE:Custom
; Filament-specific end gcode
G1 E-1 F2100 ; retract
G1 Z102 F720 ; Move print head up
G1 X178 Y178 F4200 ; park print head
G1 Z130 F720 ; Move print head further up
G4 ; wait
M104 S0 ; turn off temperature
M140 S0 ; turn off heatbed
M107 ; turn off fan
M221 S100 ; reset flow
M900 K0 ; reset LA
M84 ; disable motors
M73 P100 R0
; filament used [mm] = 18111.39
; filament used [cm3] = 227.59
; filament used [g] = 282.22
; filament cost = 10.24
; total filament used [g] = 282.22
; total filament cost = 10.24
; estimated printing time (normal mode) = 4h 13m 41s

; prusaslicer_config = begin
; avoid_crossing_perimeters = 0
; avoid_crossing_perimeters_max_detour = 0
; bed_custom_model = 
; bed_custom_texture = 
; bed_shape = 0x0,891x0,891x891,0x891
; bed_temperature = 60
; before_layer_gcode = ;BEFORE_LAYER_CHANGE\nG92 E0.0\n;[layer_z]\n\n
; between_objects_gcode = 
; bottom_fill_pattern = monotonic
; bottom_solid_layers = 1
; bottom_solid_min_thickness = 4
; bridge_acceleration = 1000
; bridge_angle = 0
; bridge_fan_speed = 100
; bridge_flow_ratio = 0.9
; bridge_speed = 500
; brim_separation = 0.1
; brim_type = outer_only
; brim_width = 0
; clip_multipart_objects = 1
; color_change_gcode = M600
; compatible_printers_condition_cummulative = "printer_model==\"MINI\" and nozzle_diameter[0]==0.8";"nozzle_diameter[0]==0.8 and ! (printer_notes=~/.*PRINTER_VENDOR_PRUSA3D.*/ and printer_notes=~/.*PRINTER_MODEL_MK(2.5|3).*/ and single_extruder_multi_material)"
; complete_objects = 0
; cooling = 1
; cooling_tube_length = 5
; cooling_tube_retraction = 91.5
; default_acceleration = 1000
; default_filament_profile = "Prusament PLA @0.8 nozzle"
; default_print_profile = 0.40mm QUALITY @0.8 nozzle MINI
; deretract_speed = 20
; disable_fan_first_layers = 1
; dont_support_bridges = 0
; draft_shield = disabled
; duplicate_distance = 6
; elefant_foot_compensation = 0.2
; end_filament_gcode = "; Filament-specific end gcode"
; end_gcode = G1 E-1 F2100 ; retract\n{if max_layer_z < max_print_height}G1 Z{z_offset+min(max_layer_z+2, max_print_height)} F720 ; Move print head up{endif}\nG1 X178 Y178 F4200 ; park print head\n{if max_layer_z < max_print_height}G1 Z{z_offset+min(max_layer_z+30, max_print_height)} F720 ; Move print head further up{endif}\nG4 ; wait\nM104 S0 ; turn off temperature\nM140 S0 ; turn off heatbed\nM107 ; turn off fan\nM221 S100 ; reset flow\nM900 K0 ; reset LA\nM84 ; disable motors
; ensure_vertical_shell_thickness = 1
; external_perimeter_extrusion_width = 4
; external_perimeter_speed = 500
; external_perimeters_first = 0
; extra_loading_move = -2
; extra_perimeters = 0
; extruder_clearance_height = 20
; extruder_clearance_radius = 45
; extruder_colour = ""
; extruder_offset = 0x0
; extrusion_axis = E
; extrusion_multiplier = 1
; extrusion_width = 4
; fan_always_on = 1
; fan_below_layer_time = 100
; filament_colour = #FF8000
; filament_cooling_final_speed = 3.4
; filament_cooling_initial_speed = 2.2
; filament_cooling_moves = 4
; filament_cost = 36.29
; filament_density = 1.24
; filament_diameter = 4
; filament_load_time = 0
; filament_loading_speed = 28
; filament_loading_speed_start = 3
; filament_max_volumetric_speed = 15
; filament_minimal_purge_on_wipe_tower = 15
; filament_notes = "Affordable filament for everyday printing in premium quality manufactured in-house by Josef Prusa"
; filament_ramming_parameters = "120 100 6.6 6.8 7.2 7.6 7.9 8.2 8.7 9.4 9.9 10.0| 0.05 6.6 0.45 6.8 0.95 7.8 1.45 8.3 1.95 9.7 2.45 10 2.95 7.6 3.45 7.6 3.95 7.6 4.45 7.6 4.95 7.6"
; filament_settings_id = "Prusament PLA @0.8 nozzle"
; filament_soluble = 0
; filament_spool_weight = 201
; filament_toolchange_delay = 0
; filament_type = PLA
; filament_unload_time = 0
; filament_unloading_speed = 90
; filament_unloading_speed_start = 100
; filament_vendor = Prusa Polymers
; fill_angle = 45
; fill_density = 0%
; fill_pattern = gyroid
; first_layer_acceleration = 800
; first_layer_acceleration_over_raft = 0
; first_layer_bed_temperature = 60
; first_layer_extrusion_width = 4
; first_layer_height = 4
; first_layer_speed = 500
; first_layer_speed_over_raft = 30
; first_layer_temperature = 225
; full_fan_speed_layer = 4
; fuzzy_skin = none
; fuzzy_skin_point_dist = 0.8
; fuzzy_skin_thickness = 0.3
; gap_fill_enabled = 1
; gap_fill_speed = 500
; gcode_comments = 0
; gcode_flavor = marlin2
; gcode_label_objects = 1
; gcode_resolution = 0.0125
; gcode_substitutions = 
; high_current_on_filament_swap = 0
; host_type = octoprint
; infill_acceleration = 1000
; infill_anchor = 2.5
; infill_anchor_max = 20
; infill_every_layers = 1
; infill_extruder = 1
; infill_extrusion_width = 4
; infill_first = 0
; infill_only_where_needed = 0
; infill_overlap = 30%
; infill_speed = 500
; interface_shells = 0
; ironing = 0
; ironing_flowrate = 15%
; ironing_spacing = 0.1
; ironing_speed = 15
; ironing_type = top
; layer_gcode = ;AFTER_LAYER_CHANGE\n;[layer_z]
; layer_height = 3
; machine_limits_usage = emit_to_gcode
; machine_max_acceleration_e = 5000
; machine_max_acceleration_extruding = 1250
; machine_max_acceleration_retracting = 1250
; machine_max_acceleration_travel = 2500
; machine_max_acceleration_x = 2500
; machine_max_acceleration_y = 2500
; machine_max_acceleration_z = 2500
; machine_max_feedrate_e = 500
; machine_max_feedrate_x = 500
; machine_max_feedrate_y = 500
; machine_max_feedrate_z = 500
; machine_max_jerk_e = 10
; machine_max_jerk_x = 8
; machine_max_jerk_y = 8
; machine_max_jerk_z = 2
; machine_min_extruding_rate = 0
; machine_min_travel_rate = 0
; max_fan_speed = 100
; max_layer_height = 0.55
; max_print_height = 891
; max_print_speed = 50
; max_volumetric_extrusion_rate_slope_negative = 0
; max_volumetric_extrusion_rate_slope_positive = 0
; max_volumetric_speed = 0
; min_bead_width = 85%
; min_fan_speed = 100
; min_feature_size = 25%
; min_layer_height = 0.2
; min_print_speed = 15
; min_skirt_length = 4
; mmu_segmented_region_max_width = 0
; notes = 
; nozzle_diameter = 4
; only_retract_when_crossing_perimeters = 0
; ooze_prevention = 0
; output_filename_format = {input_filename_base}_{nozzle_diameter[0]}n_{layer_height}mm_{initial_filament_type}_{printer_model}_{print_time}.gcode
; overhangs = 0
; parking_pos_retraction = 92
; pause_print_gcode = M601
; perimeter_acceleration = 800
; perimeter_extruder = 1
; perimeter_extrusion_width = 4
; perimeter_generator = arachne
; perimeter_speed = 500
; perimeters = 1
; physical_printer_settings_id = 
; post_process = 
; print_settings_id = 0.40mm QUALITY @0.8 nozzle MINI
; printer_model = MINI
; printer_notes = Don't remove the following keywords! These keywords are used in the "compatible printer" condition of the print and filament profiles to link the particular print and filament profiles to this printer profile.\nPRINTER_VENDOR_PRUSA3D\nPRINTER_MODEL_MINI\n
; printer_settings_id = Original Prusa MINI & MINI+ 0.8 nozzle
; printer_technology = FFF
; printer_variant = 0.8
; printer_vendor = 
; raft_contact_distance = 0.2
; raft_expansion = 1.5
; raft_first_layer_density = 90%
; raft_first_layer_expansion = 3
; raft_layers = 0
; remaining_times = 1
; resolution = 0
; retract_before_travel = 1.5
; retract_before_wipe = 70%
; retract_layer_change = 1
; retract_length = 3
; retract_length_toolchange = 4
; retract_lift = 0.2
; retract_lift_above = 0
; retract_lift_below = 179
; retract_restart_extra = 0
; retract_restart_extra_toolchange = 0
; retract_speed = 45
; seam_position = aligned
; silent_mode = 0
; single_extruder_multi_material = 0
; single_extruder_multi_material_priming = 0
; skirt_distance = 3
; skirt_height = 2
; skirts = 1
; slice_closing_radius = 0.049
; slicing_mode = regular
; slowdown_below_layer_time = 20
; small_perimeter_speed = 500
; solid_infill_below_area = 0
; solid_infill_every_layers = 0
; solid_infill_extruder = 1
; solid_infill_extrusion_width = 4
; solid_infill_speed = 500
; spiral_vase = 0
; standby_temperature_delta = -5
; start_filament_gcode = "M900 K{if printer_notes=~/.*PRINTER_MODEL_MINI.*/ and nozzle_diameter[0]==0.6}0.12{elsif printer_notes=~/.*PRINTER_MODEL_MINI.*/ and nozzle_diameter[0]==0.8}0.06{elsif printer_notes=~/.*PRINTER_MODEL_MINI.*/}0.2{elsif nozzle_diameter[0]==0.8}0.01{elsif nozzle_diameter[0]==0.6}0.04{else}0.05{endif} ; Filament gcode LA 1.5\n{if printer_notes=~/.*PRINTER_MODEL_MINI.*/};{elsif printer_notes=~/.*PRINTER_HAS_BOWDEN.*/}M900 K200{elsif nozzle_diameter[0]==0.6}M900 K18{elsif nozzle_diameter[0]==0.8};{else}M900 K30{endif} ; Filament gcode LA 1.0"
; start_gcode = M862.3 P "[printer_model]" ; printer model check\nG90 ; use absolute coordinates\nM83 ; extruder relative mode\nM104 S170 ; set extruder temp for bed leveling\nM140 S[first_layer_bed_temperature] ; set bed temp\nM109 R170 ; wait for bed leveling temp\nM190 S[first_layer_bed_temperature] ; wait for bed temp\nM204 T1250 ; set travel acceleration\nG28 ; home all without mesh bed level\nG29 ; mesh bed leveling \nM204 T[machine_max_acceleration_travel] ; restore travel acceleration\nM104 S[first_layer_temperature] ; set extruder temp\nG92 E0\nG1 Y-2 X179 F2400\nG1 Z3 F720\nM109 S[first_layer_temperature] ; wait for extruder temp\n\n; intro line\nG1 X170 F1000\nG1 Z0.2 F720\nG1 X110 E8 F900\nG1 X40 E10 F700\nG92 E0\n\nM221 S95 ; set flow
; support_material = 0
; support_material_angle = 0
; support_material_auto = 1
; support_material_bottom_contact_distance = 0
; support_material_bottom_interface_layers = 0
; support_material_buildplate_only = 0
; support_material_closing_radius = 2
; support_material_contact_distance = 0.25
; support_material_enforce_layers = 0
; support_material_extruder = 0
; support_material_extrusion_width = 0.7
; support_material_interface_contact_loops = 0
; support_material_interface_extruder = 0
; support_material_interface_layers = 2
; support_material_interface_pattern = rectilinear
; support_material_interface_spacing = 0.4
; support_material_interface_speed = 100%
; support_material_pattern = rectilinear
; support_material_spacing = 2
; support_material_speed = 500
; support_material_style = grid
; support_material_synchronize_layers = 0
; support_material_threshold = 50
; support_material_with_sheath = 0
; support_material_xy_spacing = 80%
; temperature = 225
; template_custom_gcode = 
; thick_bridges = 1
; thin_walls = 0
; threads = 12
; thumbnails = 16x16,220x124
; thumbnails_format = PNG
; toolchange_gcode = 
; top_fill_pattern = monotonic
; top_infill_extrusion_width = 4
; top_solid_infill_speed = 500
; top_solid_layers = 1
; top_solid_min_thickness = 4
; travel_speed = 500
; travel_speed_z = 500
; use_firmware_retraction = 0
; use_relative_e_distances = 1
; use_volumetric_e = 0
; variable_layer_height = 1
; wall_distribution_count = 1
; wall_transition_angle = 10
; wall_transition_filter_deviation = 25%
; wall_transition_length = 0.8
; wipe = 1
; wipe_into_infill = 0
; wipe_into_objects = 0
; wipe_tower = 1
; wipe_tower_bridging = 10
; wipe_tower_brim_width = 2
; wipe_tower_no_sparse_layers = 0
; wipe_tower_rotation_angle = 0
; wipe_tower_width = 60
; wipe_tower_x = 170
; wipe_tower_y = 140
; wiping_volumes_extruders = 70,70
; wiping_volumes_matrix = 0
; xy_size_compensation = 0
; z_offset = 0
; prusaslicer_config = end
