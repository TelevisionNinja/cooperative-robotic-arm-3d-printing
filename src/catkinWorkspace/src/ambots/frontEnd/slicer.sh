#!/bin/bash
dir $1
prusa-slicer --export-gcode $1 --load $2 --dont-arrange
exit
