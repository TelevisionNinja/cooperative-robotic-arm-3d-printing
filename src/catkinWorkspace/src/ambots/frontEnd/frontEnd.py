#!/usr/bin/env python3

# for opening files
import sys
import os
fileDirectory = os.path.realpath(__file__)
fileDirectory = os.path.dirname(fileDirectory)
sys.path.insert(0, fileDirectory)

# communication hub
import rospy
from std_msgs.msg import String

# slicing
import pymesh
import subprocess


class FrontEnd:
    def __init__(self, communication_hub_topic_name = '/ambots/communication_hub_topic', inputDirectory = "", outputDirectory = ""):
        rospy.init_node('frontEnd', anonymous=True)
        self.msg_type = String
        self.communication_hub_topic_name = communication_hub_topic_name
        self.commandPublisher = rospy.Publisher(self.communication_hub_topic_name, self.msg_type, queue_size=1000)

        # wait for subscriber
        while self.commandPublisher.get_num_connections() == 0:
            rospy.sleep(1) # seconds

        self.inputDirectory = fileDirectory + "/input"

        if inputDirectory != "":
            self.inputDirectory = inputDirectory

        self.outputDirectory = fileDirectory + "/output"

        if outputDirectory != "":
            self.outputDirectory = outputDirectory


    def startPrinting(self, robotID = 0):
        self.commandPublisher.publish('a1 r' + str(robotID))


    def sendChunkToArm(self, robotID, chunk):
        self.commandPublisher.publish('a5 r' + str(robotID) + '\n' + chunk)


    def injectCommandsCases(self, robotID, lines):
        if robotID == 0:
            lines.insert(0, "A7 R1 X0 Y1100 Z1100\n") # move robot 1 out of the way
            lines.append("\nA3 R1") # send state data to robot 1
            lines.append("\nA7 R0 X400 Y0 Z400") # move robot 0 out of the way
            lines.append("\nA1 R1") # start robot 1
            lines.append("\nA2 R0") # pause robot 0
            return
        elif robotID == 1:
            lines.append("\nA7 R1 X0 Y1100 Z1100") # move robot 1 out of the way
            lines.append("\nA6 R0") # resume robot 0
            lines.append("\nA2 R1") # pause robot 1
            return


    def injectCommands(self):
        # create the directory if it doesn't exist
        if not os.path.exists(self.outputDirectory):
            os.makedirs(self.outputDirectory)
            return

        # loop over all files in the directory
        for filename in os.listdir(self.outputDirectory):
            # check if the file ends with ".gcode"
            if filename.endswith(".gcode"):
                # get the robot id from the file name
                index = filename.rfind(".")
                robotID = int(filename[:index])

                # open the file
                filePath = os.path.join(self.outputDirectory, filename)
                with open(filePath, 'r+') as openedFile:
                    lines = openedFile.readlines()
                    self.injectCommandsCases(robotID, lines)
                    openedFile.seek(0)
                    openedFile.writelines(lines)


    def sendAllChunks(self):
        # create the directory if it doesn't exist
        if not os.path.exists(self.outputDirectory):
            os.makedirs(self.outputDirectory)
            return

        # loop over all files in the directory
        for filename in os.listdir(self.outputDirectory):
            # check if the file ends with ".gcode"
            if filename.endswith(".gcode"):
                # get the robot id from the file name
                index = filename.rfind(".")
                robotID = int(filename[:index])

                # open the file
                filePath = os.path.join(self.outputDirectory, filename)
                with open(filePath) as openedFile:
                    data = openedFile.read()
                    self.sendChunkToArm(robotID, data)


    def cutMesh(self):
        print("What file would you like to slice? (Type the name of the file. Ex: cube.stl)")
        userFilename = input()
        userFilename = self.inputDirectory + "/" + userFilename

        #load initial stl files (user uploaded) and plane used for cutting
        halfMesh = pymesh.load_mesh(self.inputDirectory + "/half.ply")
        userMesh = pymesh.load_mesh(userFilename)   

        #Making sure the Usermesh is centered (0,y,0) and that it is all on sitting on the positive side of the y axis
        num = 0
        #find minimum and maximum X vertex value, minimum Y vertex value, and minimum and maximum z vertex value
        for i in userMesh.vertices:
            count = 0
            for j in i:
                count = count + 1
                if count == 1:
                    if num == 0:
                        minX = j
                        maxX = j
                    else:
                        if j < minX:
                            minX = j
                        if j > maxX:
                            maxX = j
                elif count == 2:
                    if num == 0:
                        minY = j
                    else:
                        if j < minY:
                            minY = j
                else:
                    if num == 0:
                        minZ = j
                        maxZ = j
                    else:
                        if j <  minZ:
                            minZ = j
                        if j > maxZ:
                            maxZ = j
            num = num + 1

        #determining how y should be translated
        if minY > 0:
            yTranslation = minY
        elif minY < 0:
            yTranslation = -minY
        else:
            yTranslation = 0

        #determininy how x should be translated
        xAverage = (maxX + minX)/2.0
        if xAverage < 0:
            xTranslation = xAverage
        elif xAverage > 0:
            xTranslation = -xAverage
        else:
            xTranslation = 0

        #determining how z should be translated
        zAverage = (maxZ + minZ)/2.0
        if zAverage < 0:
            zTranslation = zAverage
        elif xAverage > 0:
            zTranslation = -zAverage
        else:
            zTranslation = 0

        #creating new mesh with translated vertices
        userMesh = pymesh.form_mesh(userMesh.vertices + [[xTranslation,yTranslation,zTranslation]], userMesh.faces)

        #boolean operations to cut mesh
        oneMesh = pymesh.boolean(halfMesh, userMesh, "intersection")
        twoMesh = pymesh.boolean(userMesh, halfMesh, operation="difference", engine="igl")

        #translate to correct coordinates
        oneMesh = pymesh.form_mesh(oneMesh.vertices + [[445.5, 445.5, 0]], oneMesh.faces)
        twoMesh = pymesh.form_mesh(twoMesh.vertices + [[445.4, 445.4, 0]], twoMesh.faces)

        # create the directory if it doesn't exist
        if not os.path.exists(self.outputDirectory):
            os.makedirs(self.outputDirectory)

        #save stl files
        pymesh.save_mesh(self.outputDirectory + "/0.stl", oneMesh, save_normal_vertex=False)
        pymesh.save_mesh(self.outputDirectory + "/1.stl", twoMesh, save_normal_vertex=False)


    def cutDemoMesh(self):
        #load initial stl files (user uploaded) and plane used for cutting
        halfMesh = pymesh.load_mesh(self.inputDirectory + "/half.ply")
        userMesh = pymesh.load_mesh(self.inputDirectory + "/cube.stl")   

        #Making sure the Usermesh is centered (0,y,0) and that it is all on sitting on the positive side of the y axis
        num = 0
        #find minimum and maximum X vertex value, minimum Y vertex value, and minimum and maximum z vertex value
        for i in userMesh.vertices:
            count = 0
            for j in i:
                count = count + 1
                if count == 1:
                    if num == 0:
                        minX = j
                        maxX = j
                    else:
                        if j < minX:
                            minX = j
                        if j > maxX:
                            maxX = j
                elif count == 2:
                    if num == 0:
                        minY = j
                    else:
                        if j < minY:
                            minY = j
                else:
                    if num == 0:
                        minZ = j
                        maxZ = j
                    else:
                        if j <  minZ:
                            minZ = j
                        if j > maxZ:
                            maxZ = j
            num = num + 1

        #determining how y should be translated
        if minY > 0:
            yTranslation = minY
        elif minY < 0:
            yTranslation = -minY
        else:
            yTranslation = 0

        #determininy how x should be translated
        xAverage = (maxX + minX)/2.0
        if xAverage < 0:
            xTranslation = xAverage
        elif xAverage > 0:
            xTranslation = -xAverage
        else:
            xTranslation = 0

        #determining how z should be translated
        zAverage = (maxZ + minZ)/2.0
        if zAverage < 0:
            zTranslation = zAverage
        elif xAverage > 0:
            zTranslation = -zAverage
        else:
            zTranslation = 0

        #creating new mesh with translated vertices
        userMesh = pymesh.form_mesh(userMesh.vertices + [[xTranslation,yTranslation,zTranslation]], userMesh.faces)

        #boolean operations to cut mesh
        oneMesh = pymesh.boolean(halfMesh, userMesh, "intersection")
        twoMesh = pymesh.boolean(userMesh, halfMesh, operation="difference", engine="igl")

        #translate to correct coordinates
        oneMesh = pymesh.form_mesh(oneMesh.vertices + [[445.5, 445.5, 0]], oneMesh.faces)
        twoMesh = pymesh.form_mesh(twoMesh.vertices + [[445.4, 445.4, 0]], twoMesh.faces)

        # create the directory if it doesn't exist
        if not os.path.exists(self.outputDirectory):
            os.makedirs(self.outputDirectory)

        #save stl files
        pymesh.save_mesh(self.outputDirectory + "/0.stl", oneMesh, save_normal_vertex=False)
        pymesh.save_mesh(self.outputDirectory + "/1.stl", twoMesh, save_normal_vertex=False)


    def slice(self):
        #runs the bash file that uses prusa slicer to slice into gcode
        subprocess.run([fileDirectory + "/slicer.sh " + self.outputDirectory + "/0.stl " + fileDirectory + "/config.ini"], shell=True)
        subprocess.run([fileDirectory + "/slicer.sh " + self.outputDirectory + "/1.stl " + fileDirectory + "/config.ini"], shell=True)


    def userInterface(self):
        # command handler
        while True:
            print("Type the number of the command to execute the command. Always press the enter key to enter in the command/input:")
            print("\t1) Start demo (This will select, slice, send, and start a print job to the arms)")
            print("\t2) Slice an STL file")
            print("\t3) Send G Code to the arms")
            print("\t4) Start printing")
            print("\t5) Quit")

            selection = input()

            # command 1
            if selection == "1":
                self.cutDemoMesh()
                self.slice()
                self.injectCommands()
                self.sendAllChunks()
                self.startPrinting()

            # command 2
            elif selection == "2":
                self.cutMesh()
                self.slice()
                self.injectCommands()

            # command 3
            elif selection == "3":
                self.sendAllChunks()

            # command 4
            elif selection == "4":
                # start arm 0
                self.startPrinting()

            # command 5
            elif selection == "5":
                print("Goodbye!")
                return

            # unknown command
            else:
                print("That is not a command")

            print()


def main():
    frontEnd = FrontEnd()
    frontEnd.userInterface()


if __name__ == '__main__':
    main()
