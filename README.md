# AMBOTS Cooperative Robotic Arm 3D Printing

## Description
The objective of this project is to perform cooperative 3D printing with industrial robotic arms with ROS. This is done using the Kinova Gen 3 6-DOF robotic arm, and the Universal Robot's UR10. The user has the capability to upload an STL (or OBJ or PLY) file and the resulting simulation will have both brands of robotic arm printing half of the object in the same RViz simulation environment. The arms communicate through the communication hub and the G-Code commands are processed through each arm's respective firmware.

## Visuals
![cube](./docs/img/cube1.png)
![cube](./docs/img/cube2.png)
![cube](./docs/img/cube3.jpg)
![cube](./docs/img/cube4.jpg)
![cube](./docs/img/cube5.jpg)

![jet](./docs/img/jet1.png)
![jet](./docs/img/jet2.png)
![jet](./docs/img/jet3.png)
![jet](./docs/img/jet4.png)
![jet](./docs/img/jet5.png)
![jet](./docs/img/jet6.png)
![jet](./docs/img/jet7.png)

</br>

Time-Lapse Video:
</br>
<div align="center">
  <a href="https://www.youtube.com/watch?v=4LDEwEDrnR4"><img src="https://img.youtube.com/vi/4LDEwEDrnR4/0.jpg" alt="youtube thumbnail of time-lapse video"></a>
</div> 

</br>

How to Use Video:
</br>
<div align="center">
  <a href="https://www.youtube.com/watch?v=PnNFoZnCxrM"><img src="https://img.youtube.com/vi/PnNFoZnCxrM/0.jpg" alt="youtube thumbnail of time-lapse video"></a>
</div> 

## Requirements
As is, this project can only be run in Ubuntu 20.04. No other versions of Ubuntu or distributions of Linux have been tested. All other system dependencies are in the Installation section.

## Installation
The Installation instructions can also be found in `/docs/install.md`. The install script and instructions will set up a specific directory structure that this project refers to and uses in the documentation and code.

### Install via Install Script
1. Download the script `install.sh` from the scripts folder
</br>

On GitLab, click on the `src` folder

Click on the `scripts` folder

Click on the `install.sh` file

Click download

The `install.sh` file should now be in your downloads folder. Depending on your system and settings, the download location may be different. In the step 2 example, we assume the `install.sh` script has been saved to the `Downloads` folder

</br>

2. Navigate to the directory where the `install.sh` script is downloaded

</br>

Example: (In this example, the `install.sh` file has been downloaded to the `Downloads` folder)
```bash
# this is an example of navigating to the location of the install.sh script
cd /Downloads
```
</br>

3. Make the script executable by executing this command:
```bash
sudo chmod +x install.sh
```

4. Run the script:
</br>

Follow any directions in the terminal such as entering your root user password when prompted
```bash
./install.sh
```

</br>

### Manually Install:

Following these instructions below, in order, will result in the same install as using the `install.sh` script

</br>

#### Install ROS 1 Noetic:
```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install ros-noetic-desktop-full
source /opt/ros/noetic/setup.bash
sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
sudo apt install python3-rosdep
sudo rosdep init
rosdep update
```

</br>

#### Install MoveIt for ROS 1 Noetic:
```bash
sudo apt install ros-noetic-moveit
```

</br>

#### Clone the Kinova Robotics Repository for ROS 1 Noetic:
https://github.com/Kinovarobotics/ros_kortex

```bash
source /opt/ros/noetic/setup.bash
sudo apt install python3 python3-pip
sudo python3 -m pip install conan
conan config set general.revisions_enabled=1
conan profile new default --detect > /dev/null
conan profile update settings.compiler.libcxx=libstdc++11 default
mkdir -p catkin_workspace/src
cd catkin_workspace/src
git clone -b noetic-devel https://github.com/Kinovarobotics/ros_kortex.git
```

</br>

#### Clone the Ambots Repository for ROS 1 Noetic:
```bash
git clone https://gitlab.com/capstone-team16/ambots.git
```

</br>

#### Build the catkin workspace:
```bash
cd ../
rosdep install --from-paths src --ignore-src -y --rosdistro noetic
catkin_make
source devel/setup.bash
```

</br>

#### Install Universal Robots for ROS 1 Noetic:
```bash
sudo apt-get install ros-noetic-universal-robots
```

</br>

### Install PyMesh
```bash
git clone https://github.com/PyMesh/PyMesh.git
cd PyMesh
git submodule update --init
sudo apt-get purge python
sudo apt-get install \
    libeigen3-dev \
    libgmp-dev \
	libgmpxx4ldbl \
	libmpfr-dev \
	libboost-dev \
	libboost-thread-dev \
	libtbb-dev \
	python3-dev \
	python3-setuptools \
	python3-numpy \
	python3-scipy \
	python3-nose \
	python3-pip \
	cmake
```

</br>

All the test cases should pass
```bash
python3 ./setup.py build
python3 ./setup.py install --user
python3 -c "import pymesh; pymesh.test()"
```

</br>

#### Install PrusaSlicer
```bash
sudo snap install prusa-slicer
```

#### Make the slicing script executable
```bash
cd ../
cd ./src/ambots/src/catkinWorkspace/src/ambots/frontEnd/
sudo chmod +x slicer.sh
```

#### Make the launch scripts executable
```bash
cd ../../../../
cd ./scripts/
sudo chmod +x launchCommunicationHub.sh
sudo chmod +x launchFrontEnd.sh
sudo chmod +x launchKinovaFirmware.sh
sudo chmod +x launchUR10Firmware.sh
sudo chmod +x launchRviz.sh
```

</br>

## Launching the Software

### Information to Know Before Launching the Software

Make sure you have installed the code using the install script or followed the instructions in the install document.

You must launch the software in this order:
1. Rviz
2. The firmware for both the arms
3. The communication hub
4. The front end

</br>

### Launch Using Provided Scripts

For every piece of software you want to launch using our launch scripts, you must navigate to the `scripts` folder in the cloned `ambots` repository that is located in the `catkin_workspace` folder.

Example:
```bash
# this is an example of navigating to the location of the "scripts" folder from your home directory
cd ~/catkin_workspace/src/ambots/src/scripts/
```

</br>

#### Launch RViz:

Run the script:
```bash
./launchRviz.sh
```

</br>

#### Launch The Kinova Firmware:

Run the script:
```bash
./launchKinovaFirmware.sh
```

</br>

#### Launch The UR10 Firmware:

Run the script:
```bash
./launchUR10Firmware.sh
```

</br>

#### Launch The Communication Hub:

Run the script:
```bash
./launchCommunicationHub.sh
```

</br>

#### Launch The Front End:

Run the script:
```bash
./launchFrontEnd.sh
```

</br>

### Or Manually Launch:

You can launch the pieces of software like any other ROS software. You do not have to use our scripts to launch them.

</br>

#### Example of Launching RViz:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots bothArmsRvizLaunch.launch
```

</br>

#### Example of Launching The Kinova Firmware:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots kinovaFirmware.launch
```

</br>

#### Example of Launching The UR10 Firmware:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots urFirmware.launch
```

</br>

#### Example of Launching The Communication Hub:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots communicationHub.launch
```

</br>

#### Example of Launching The Front End:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots frontEnd.launch
```

</br>

## How To Use the Front End

The Front End will display a list of commands that one can execute:

1. Start demo
2. Slice an STL file
3. Send G code to the arms
4. Start printing
5. Quit

### How To Select a Command:

Type the number of the command. The number of a command is shown on the left side of the command in the terminal. Press the enter key to execute the command you have typed.

When a command has finished executing, the list of commands will be displayed again. You can then execute another command.

</br>

### Command 1: Demo

If number 1 is selected, you will not be able to choose the file that is printed. Instead, a demo object, which is a cube from the “cube.stl” file, is used. However, if you want to specify a specific STL file to slice and print, one has to execute commands number 2, 3, and 4. The “cube.stl” file is automatically cut into two halves. These halves are then sliced and saved into G-Code files using a slicer script. These G-Code files are sent to the arms. A start command is then sent to the Kinova arm to start printing. 

</br>

### Command 2: Slicing

When this command is executed, you will be prompted to type the name of the STL file that will be sliced. Only the files in the "input" folder can be sliced. If you provide a file path or a file name of a file that is not in the "input" folder, the front end will throw an error and terminate. Once you have typed the file name of the file you want to slice, press the enter key. This will start the slicing process. The output will be saved to the "output" folder.

The front end also has the capability to accept other CAD file formats such as OBJ and PLY. To specify your own file to be sliced, add your file to the "input" folder. Then execute command 2 and type the name of the file you added into the folder.

</br>

### Command 3: Sending G-Code

When command 3 is executed, the two G-Code files are sent to the communication hub. Execute this command to send the generated G-Code to the arms when you have sliced an STL file of your choosing.

</br>

### Command 4: Start Printing

When this command is executed, the front end will send the A1 command to tell the Kinova arm to start printing. Execute this command to start printing when you have sliced and sent the G-Code to the arms.

</br>

### Command 5: Quit

This command will exit out of the front end. The other software, such as RViz, both of the firmware, and the communication hub, will continue to run.

</br>

## License
This project is available under the MIT license. See the LICENSE folder for more information.
