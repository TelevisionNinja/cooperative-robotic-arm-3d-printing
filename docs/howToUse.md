# How To Use the Front End

The Front End will display a list of commands that one can execute:

1. Start demo
2. Slice an STL file
3. Send G code to the arms
4. Start printing
5. Quit

## How To Select a Command:

Type the number of the command. The number of a command is shown o the left side of the command in the terminal. Press the enter key execute the command you have typed.

When a command has finished executing, the list of commands will be displayed again. You can then execute another command.

</br>

## Comamnd 1: Demo

If number 1 is selected, the user will not be able to choose the file that is printed. Instead, a demo object, which is a cube from the “cube.stl” file, is used. However, if one wants to specify a specific STL file to slice and print, one has to execute commands number 2, 3, and 4. The “cube.stl” file is automatically cut into 2 haves. These halves are then sliced and saved into G-Code files using a slicer script. These G-Code files are sent to the arms. A start command is then sent to the Kinova arm to start printing. 

</br>

## Comamnd 2: Slicing

When this command is executed, you will be prompted to type the name of the STL file that will be sliced. Only the files in the "input" folder will be sliced. If you provide a file path or a file name of a file that is not in the "input" folder, the front end will throw and error and terminate. Once you have typed the file name of the file you want to slice, press the enter key. This will start the slicing process. The output will be saved to the "output" folder.

The front end also has the capability to accept other CAD file formats such as OBJ and PLY. To specify your own file to be sliced, add your file to the "input" folder. Then execute command 2 and type the name of the file you added into the folder.

</br>

## Comamnd 3: Sending G-Code

When command 3 is executed, the two G-Code files are then sent to the communication hub. Execute this command to send the generated G-Code to the arms when you have sliced an STL file of your choosing.

</br>

## Comamnd 4: Start Printing

When this command is executed, the front end will send the A1 command to tell the Kinova arm to start printing. Execute this command to start printing when you have sliced and sent the G-Code to the arms.

</br>

## Comamnd 5: Quit

This command will exit out of the front end. The other software, such as RViz, both of the firmware, and the communication hub, will continue to run.
