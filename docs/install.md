# Before You Install:
Make sure you are using Ubuntu version 20.04. The install script and instructions will set up a specific directory structure that this project refers to and uses in the documentation and code.

</br>

# Install:
1. Download the script `install.sh` from the scripts folder
</br>

On GitLab, click on the `src` folder

Click on the `scripts` folder

Click on the `install.sh` file

Click download

The `install.sh` file should now be in your downloads folder. Depending on your system and settings, the download location may be different. In the step 2 example, we assume the `install.sh` script has been saved to the `Downloads` folder

</br>

2. Navigate to the directory where the `install.sh` script is downloaded

</br>

Example: (In this example, the `install.sh` file has been downloaded to the `Downloads` folder)
```bash
# this is an example of navigating to the location of the install.sh script
cd /Downloads
```
</br>

3. Make the script executable by executing this command:
```bash
sudo chmod +x install.sh
```

4. Run the script:
</br>

Follow any directions in the terminal such as entering your root user password when prompted
```bash
./install.sh
```

</br>

# Or Manually Install:

Following these instructions below, in order, will result in the same install as using the `install.sh` script

</br>

## Install ROS 1 Noetic:
```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
sudo apt install ros-noetic-desktop-full
source /opt/ros/noetic/setup.bash
sudo apt install python3-rosdep python3-rosinstall python3-rosinstall-generator python3-wstool build-essential
sudo apt install python3-rosdep
sudo rosdep init
rosdep update
```

</br>

## Install MoveIt for ROS 1 Noetic:
```bash
sudo apt install ros-noetic-moveit
```

</br>

## Clone the Kinova Robotics Repository for ROS 1 Noetic:
https://github.com/Kinovarobotics/ros_kortex

```bash
source /opt/ros/noetic/setup.bash
sudo apt install python3 python3-pip
sudo python3 -m pip install conan
conan config set general.revisions_enabled=1
conan profile new default --detect > /dev/null
conan profile update settings.compiler.libcxx=libstdc++11 default
mkdir -p catkin_workspace/src
cd catkin_workspace/src
git clone -b noetic-devel https://github.com/Kinovarobotics/ros_kortex.git
```

</br>

## Clone the Ambots Repository for ROS 1 Noetic:
```bash
git clone https://gitlab.com/capstone-team16/ambots.git
```

</br>

## Build the catkin workspace:
```bash
cd ../
rosdep install --from-paths src --ignore-src -y --rosdistro noetic
catkin_make
source devel/setup.bash
```

</br>

## Install Universal Robots for ROS 1 Noetic:
```bash
sudo apt-get install ros-noetic-universal-robots
```

</br>

# Install PyMesh
```bash
git clone https://github.com/PyMesh/PyMesh.git
cd PyMesh
git submodule update --init
sudo apt-get purge python
sudo apt-get install \
    libeigen3-dev \
    libgmp-dev \
	libgmpxx4ldbl \
	libmpfr-dev \
	libboost-dev \
	libboost-thread-dev \
	libtbb-dev \
	python3-dev \
	python3-setuptools \
	python3-numpy \
	python3-scipy \
	python3-nose \
	python3-pip \
	cmake
```

</br>

All the test cases should pass
```bash
python3 ./setup.py build
python3 ./setup.py install --user
python3 -c "import pymesh; pymesh.test()"
```

</br>

# Install PrusaSlicer
```bash
sudo snap install prusa-slicer
```

## Make the slicing script executable
```bash
cd ../
cd ./src/ambots/src/catkinWorkspace/src/ambots/frontEnd/
sudo chmod +x slicer.sh
```

# Make the launch scripts executable
```bash
cd ../../../../
cd ./scripts/
sudo chmod +x launchCommunicationHub.sh
sudo chmod +x launchFrontEnd.sh
sudo chmod +x launchKinovaFirmware.sh
sudo chmod +x launchUR10Firmware.sh
sudo chmod +x launchRviz.sh
```
