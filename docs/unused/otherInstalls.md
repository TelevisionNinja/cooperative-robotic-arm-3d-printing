Tested Ubuntu Version: 22.04

## Install ROS 2 Galactic:
```bash
sudo apt update && sudo apt install curl gnupg lsb-release
sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(source /etc/os-release && echo $UBUNTU_CODENAME) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null
sudo apt update
sudo apt upgrade
sudo apt install ros-galactic-desktop
source /opt/ros/galactic/setup.bash
```

</br>

## Install MoveIt for ROS 2 Galactic:
```bash
sudo apt install ros-galactic-moveit
```

</br>

## Install Universal Robots for ROS 2 Galactic:
https://github.com/UniversalRobots/Universal_Robots_ROS2_Driver

```bash
source /opt/ros/galactic/setup.bash
sudo apt install python3-colcon-common-extensions python3-vcstool
export COLCON_WS=~/workspace/ros_ur_driver
mkdir -p $COLCON_WS/src
cd $COLCON_WS
git clone -b galactic https://github.com/UniversalRobots/Universal_Robots_ROS2_Driver.git src/Universal_Robots_ROS2_Driver
vcs import src --skip-existing --input src/Universal_Robots_ROS2_Driver/Universal_Robots_ROS2_Driver.galactic.repos
rosdep update
rosdep install --ignore-src --from-paths src -y -r --rosdistro galactic
colcon build --cmake-args -DCMAKE_BUILD_TYPE=Release
source install/setup.bash
```

</br>

## Install Universal Robots Gazebo for ROS 2 Galactic:
https://github.com/UniversalRobots/Universal_Robots_ROS2_Gazebo_Simulation

```bash
source /opt/ros/galactic/setup.bash
export COLCON_WS=~/workspace/ros_ws_galactic
mkdir -p $COLCON_WS/src
cd $COLCON_WS
git clone -b fix_dependencies https://github.com/UniversalRobots/Universal_Robots_ROS2_Gazebo_Simulation.git
vcs import src --input Universal_Robots_ROS2_Gazebo_Simulation/Universal_Robots_ROS2_Gazebo_Simulation.galactic.repos
rosdep install --ignore-src --from-paths src -y -r --rosdistro galactic
colcon build --symlink-install
source install/setup.bash
```

Edit these 2 files:</br>
```
~/workspace/ros_ws_galactic/install/ur_description/share/ur_description/urdf/inc/ur_common.xacro
```

```
~/workspace/ros_ws_galactic/install/ur_description/share/ur_description/urdf/ur.urdf.xacro
```

Change the lines within the files:</br>
"xacro.load_yaml" needs to be "load_yaml"

</br>

## Install M3DP-sim:
### Install the dependencies
```bash
# install gazebo dependencies
sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'
wget https://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add -
sudo apt-get update
wget https://raw.githubusercontent.com/ignition-tooling/release-tools/master/jenkins-scripts/lib/dependencies_archive.sh -O /tmp/dependencies.sh
GAZEBO_MAJOR_VERSION=11 ROS_DISTRO=galactic . /tmp/dependencies.sh
echo $BASE_DEPENDENCIES $GAZEBO_BASE_DEPENDENCIES | tr -d '\\' | xargs sudo apt-get -y install

# install build essentials
sudo apt update
sudo apt install build-essentials 

# install M3DP-sim's provided gazebo
git clone https://bitbucket.org/hbpneurorobotics/gazebo.git
cd gazebo
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=$HOME/.local ..
make -j8
make install
```

### Install Gazebo Fluid Simulation Plugin with SPlisHSPlasH
Run:
```bash
nano ~/.bashrc
```

Add a line to the bottom of the file:
```
export LD_LIBRARY_PATH=/home/jason2/.local/lib:$LD_LIBRARY_PATH:/home/<your_User_Name>/splisplash/build/lib
```

Run:
```bash
source ~/.bashrc
```

Run:
```bash
git clone https://github.com/ai4ce/M3DP-Sim.git
```

Run:
```bash
git clone https://github.com/doyubkim/fluid-engine-dev
cd fluid-engine-dev/
mkdir build && cd build && cmake .. && make
```
