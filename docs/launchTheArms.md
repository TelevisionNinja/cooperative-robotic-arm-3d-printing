## Launch ROS 2 RVIZ with the UR10 Arm:
```bash
source /opt/ros/galactic/setup.bash
source ~/ws_moveit2/install/setup.bash
cd ~/workspace/ros_ur_driver
source install/setup.bash
ros2 launch ur_moveit_config ur_moveit.launch.py ur_type:=ur10 launch_rviz:=true
```

</br>

## launch ROS 2 Gazebo with the UR10 Arm:
```bash
source /opt/ros/galactic/setup.bash
cd ~/workspace/ros_ws_galactic
source install/setup.bash
ros2 launch ur_simulation_gazebo ur_sim_moveit.launch.py ur_type:=ur10 launch_rviz:=false
```

</br>

## Launch ROS 1 Gazebo with the Kinova Arm:
```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch kortex_gazebo spawn_kortex_robot.launch
```

</br>

## Launch ROS 1 Gazebo with the UR10 Arm:
```bash
source /opt/ros/noetic/setup.bash
cd ur_catkin_ws
source devel/setup.bash
roslaunch ur_gazebo ur10.launch
```

</br>

## Launch the ROS 1 Kinova Arm MoveIt Example:
```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch kortex_examples moveit_example.launch
```
