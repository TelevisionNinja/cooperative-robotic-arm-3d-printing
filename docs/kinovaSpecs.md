max speed: 500 mm/s
max reach: 891 mm / 902 mm

assume 4 mm diameter filament
assume 4 mm diameter nozzle

named positions:
'home'
'retract'
'vertical'

# Print Settings

## Layers and perimeters

### Layer Height
layer height: 3 mm
first layer height: 4 mm

### Verticle Shells
perimeters minimum: 1

### Horizontal Shells
solid layers top: 1
solid layers bottom: 1
minimum shell thickness top: 4 mm
minimum shell thickness bottom: 4 mm

## Infill
### Infill
fill density: 0%

## Speed
### Speed for print moves
set all to 500 mm/s

### Speed for non-print moves
set all to 500 mm/s

### Modifiers
first layer speed: 500 mm/s

## Advanced
### Extrusion Width
set all to 4 mm

# Filament Settings
## Filament
### Filament
diameter: 4 mm

# Printer Settings
## General
### Size and coordinates
max print height: 891 mm

## Mahine limits
### maximum feedrates
set all to 500 mm/s

### maximum feedrates
maximum acceleration Z: set to the same value as maximum acceleration X and maximum acceleration Y

## Extruder 1
### Size
nozzle diameter: 4 mm
