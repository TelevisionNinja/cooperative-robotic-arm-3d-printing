## Requirements Documentation

### Objective
The objective of this project is to simulate 3D printing using with at least two different brands of robotic arms working together. The entire process should begin with a CAD file and end with a printed object having been simulated.

### Module 1: Frontend

The frontend should allow for the upload of a CAD file (.stl) from the user. The frontend can be as simple as a command line interface, but it preferably should have a GUI. It should use an open source slicer to convert the CAD file into an initial G-Code file. The G-Code file should go through a post-processor to be split into a file for each robotic arm. These files will then be outputted to the communication hub.

### Module 2: Communication Hub
The Communication Hub is where all of the communication should happen. This is where communication using ROS will be sent to the arms telling them to pause and resume. The robotic arms will send ROS communication back to the communication hub as well as telling the status of the print. 

### Module 3: Firmware
The Firmware should convert the G-Code recieved from the communication hub into the robotic arms native language. It should use MoveIt to calculate the robotic arms' trajectories. This is where the customization to each brand of robotic arm takes place. 

### Module 4: RViz
RViz should be used to simulate the movement of the robotic arms along with the extrusion of the filament. 