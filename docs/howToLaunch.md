# Information to Know Before Launching The Software

Make sure you have installed the code using the install script or followed the instructions in the install document.

You must launch the software in this order:
1. Rviz
2. The firmware for both the arms
3. The communication hub
4. The front end

</br>

# Launch Using Provided Scripts

For every piece of software you want to launch using our launch scripts, you must navigate to the `scripts` folder in the cloned `ambots` repository that is located in the `catkin_workspace` folder.

Example:
```bash
# this is an example of navigating to the location of the "scripts" folder from your home directory
cd ~/catkin_workspace/src/ambots/src/scripts/
```

</br>

## Launch RViz:

Run the script:
```bash
./launchRviz.sh
```

</br>

## Launch The Kinova Firmware:

Run the script:
```bash
./launchKinovaFirmware.sh
```

</br>

## Launch The UR10 Firmware:

Run the script:
```bash
./launchUR10Firmware.sh
```

</br>

## Launch The Communication Hub:

Run the script:
```bash
./launchCommunicationHub.sh
```

</br>

## Launch The Front End:

Run the script:
```bash
./launchFrontEnd.sh
```

</br>

# Or Manually Launch:

You can launch the pieces of software like any other ROS software. You do not have to use our scripts to launch them.

</br>

## Example of Launching RViz:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots bothArmsRvizLaunch.launch
```

</br>

## Example of Launching The Kinova Firmware:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots kinovaFirmware.launch
```

</br>

## Example of Launching The UR10 Firmware:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots urFirmware.launch
```

</br>

## Example of Launching The Communication Hub:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots communicationHub.launch
```

</br>

## Example of Launching The Front End:

```bash
source /opt/ros/noetic/setup.bash
cd catkin_workspace
source devel/setup.bash
roslaunch ambots frontEnd.launch
```
